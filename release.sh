#!/bin/bash

make -j "$(nproc)" -C depends/

./autogen.sh
./configure --prefix=`pwd`/depends/x86_64-pc-linux-gnu/ --disable-tests --disable-bench --disable-maintainer-mode --disable-dependency-tracking --disable-ccache

make -j "$(nproc)"

mkdir install

make -j "$(nproc)" install DESTDIR=`pwd`/install
