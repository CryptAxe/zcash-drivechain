## Using zSide in Regtest Mode
## Dec 31, 2020 -- Felix Ha

### 0. Setup 

    # Edit paths to your drivenetd, zsided, drivenet-cli and zside-cli

    # Start Drivenet daemon in regtest mode:
    # Terminal window 1
    # cd ~/Desktop/mainchain/src/
    # ./drivenetd -regtest -printtoconsole -rpcuser=user -rpcpassword=pass -server=1 -minerbreakforbmm=1 -rpcport=18443
    #
    # Start zSide daemon in regtest mode:
    # Terminal window 2
    # cd ~/Desktop/zcash-drivechain/src/
    # ./zsided -regtest -printtoconsole -rpcuser=user -rpcpassword=pass -mainchainrpcport=18443 -rpcport=2048

    # Then drag this file into a new terminal window 3 and hit enter


### 1. Start CLI Connection

    Main="./mainchain/src/drivenet-cli -rpcuser=user -rpcpassword=pass -rpcport=18443 "
    zSide="./zcash-drivechain/src/zside-cli -rpcuser=user -rpcpassword=pass -rpcport=2048 "

    echo "Checking if connection works..."
    $Main getmininginfo
    $zSide getmininginfo  
    
    
### 2. Get zSide Address

    st_adr=$( $zSide getnewaddress "" legacy )
    echo "Transparent zSide Address: " $st_adr
    

### 3. Get Main-to-Side Deposit Address    

    zSide_dep_address_formatted=$( $zSide formatdepositaddress $st_adr )
    echo "Main-to-Side Deposit Address: " $zSide_dep_address_formatted
    
    
### 4. Start Mainchain + Activate Sidechains    
    
    echo "Starting Mainchain + Activating Sidechains..."
    
    $Main generate 100
    $Main listactivesidechains
    $Main createsidechainproposal "Testchain" "copy of bitcoin core" "0186ff51f527ffdcf2413d50bdf8fab1feb20e5f82815dad48c73cf462b8b313"
    $Main createsidechainproposal "Trainchain" "sidechain for memes" "6636970264b02297c62d67b5d7a6db13eff9ec8cda73208481d70a461a5b05d0"
    $Main createsidechainproposal "Thunder" "big block sidechain" "0705ad97d60076757da514293e86a3c5686d2b3e52d2309aee36a13a548e7d30"
    $Main createsidechainproposal "Zside" "zcash sidechain" "e1b9ae0e6ceebb2d92143f7a7e5561d646a4cfea12d512683f77eb16b7e5385f"
    $Main listsidechainproposals

    echo "Mining enough blocks to activate the sidechains..."
    $Main generate 255
    $Main listactivesidechains
    
    
### 5. Mine Sidechain Blocks

    # mine sidechain blocks
    BMMAMOUNT=0.0001
    
    function sGenerate(){
      # generates n sidechain blocks (and n mainchain blocks)
      arg1=$1
      echo $arg1
      for ((n=0;n<$arg1;n++))
        do
          $Main generate 1      
          $zSide refreshbmm $BMMAMOUNT
          sleep 0.1
          $zSide getmininginfo
        done
       }
       
    sGenerate 1        
    
### 6. Make Deposit

    echo "depositing to sidechain..."

    $Main createsidechaindeposit 3 $zSide_dep_address_formatted 77 0.01
    
    # Mine enough blocks to mature the deposit
    sGenerate 7
    
    # Now we have 77 coins on the sidechain.
    

### 7. Check Deposit

    echo "Your deposit: "
    $zSide z_getbalance $st_adr
    
    
### 8. Get new shielded address
    
    sz_adr=$( $zSide z_getnewaddress ) 
    echo "Shielded zSide Address: " $sz_adr
    
    
### 10. Send transaction to shielded address

    echo "Sending to shielded address: "
    $zSide z_sendmany $st_adr '[{"address": "'$sz_adr'" ,"amount": 0.95}]'
    
    
### 11. Shielding process

    $zSide z_getoperationstatus
    
    echo "Mining enough blocks for shielding process... "
    sGenerate 7
    
    $zSide z_getoperationstatus
    
    
### 12. Check shielded balance

    echo "Your shielded balance: "
    $zSide z_getbalance $sz_adr
