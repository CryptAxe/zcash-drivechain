# Build steps for Linux

# 1. Install Dependencies

    sudo apt install -y build-essential libtool autotools-dev automake \
    pkg-config bsdmainutils python3 libevent-dev libboost-all-dev \
    libssl-dev libdb-dev libdb++-dev libsodium-dev curl git

## Optional: Install Qt dependencies (for GUI)

    sudo apt install -y libprotobuf-dev libqrencode-dev \
    libqt5core5a libqt5dbus5 libqt5gui5 protobuf-compiler \
    qttools5-dev qttools5-dev-tools

# 2. Download the Source Code

    git clone https://gitlab.com/CryptAxe/zcash-drivechain.git

# 3. Fetch & setup zcash params (only needs to happen once per system)

    cd zcash-drivechain/
    zcutil/fetch-params.sh

# 4. Setup Rust (Once per system)

    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    source $HOME/.cargo/env
    rustc --version  # Should output something like rustc 1.46.0 (04488afe3 2020-08-24)


# 5. Build

    ./autogen.sh
    ./configure --with-incompatible-bdb
    make

# 6. Run

    # daemon (for use with zside-cli)
    ./src/zsided

    # OR
    # Graphical user interface -- QT
    ./src/qt/zside-qt

# 7. Note: How to Use zcash-cli

[This file](https://gitlab.com/CryptAxe/zcash-drivechain/-/blob/master/regtest_guide_main_to_side_to_shielded.sh) demonstrates usage of zside-cli, specifically: [1] getting zside-cli to talk to zsided, [2] making t-addresses, [3] sending coins from drivenet blockchain to zside blockchain ("depositing"), [4] sending coins from t-address to z-address ("shielding" the coins).

See also the [drivechain integration script](https://github.com/drivechain-project/DriveChainIntegration/blob/master/drivechainintegration.sh) for a guide to depositing and withdrawing from sidechains via the cli.
