// Copyright (c) 2017-2020 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_PRIMITIVES_SIDECHAIN_H
#define BITCOIN_PRIMITIVES_SIDECHAIN_H

#include <amount.h>
#include <merkleblock.h>
#include <primitives/transaction.h>
#include <pubkey.h>
#include <script/script.h>
#include <serialize.h>
#include <uint256.h>

#include <limits.h>
#include <string>
#include <vector>

//
//
//
//
// TODO note to sidechain developers:
//
// You must update THIS_SIDECHAIN to the sidechain number that gets
// assigned to this sidechain once activated.
//
// BUILD_COMMIT_HASH & BUILD_TAR_HASH all may be set by you.
//
// You must also update the genesis block, port numbers (including rpc server)
// magic bytes, data directory, checkpoint blocks, and other chainparams.
//
//
//
//
//

/* Sidechain Identifiers */

//! Sidechain number
static const unsigned int THIS_SIDECHAIN = 1;

//! Sidechain build commit hash
static const std::string SIDECHAIN_BUILD_COMMIT_HASH = "1604da4d0d2334c8d01af08391feb873bae18d13";

//! Sidechain build tar hash
static const std::string SIDECHAIN_BUILD_TAR_HASH = "c40dcc59642d5adcafb87ff0c61490386772a1c99e0adef302f1e992c9cc26c3";

//! Required workscore for mainchain payout
static const int MAINCHAIN_WITHDRAWAL_BUNDLE_MIN_WORKSCORE = 131;

//! Minimum number of pooled withdrawals to create new bundle
static const unsigned int DEFAULT_MIN_WITHDRAWAL_CREATE_BUNDLE = 10;

// Temporary testnet value
static const int WITHDRAWAL_BUNDLE_FAIL_WAIT_PERIOD = 20;
// Real value final release: static const int WITHDRAWAL_BUNDLE_FAIL_WAIT_PERIOD = 144;

// The destination string for the change of a bundle
static const std::string SIDECHAIN_WITHDRAWAL_BUNDLE_RETURN_DEST = "D";

// The bill time format (until upgrading Qt versions so we can use epoch time)
static const std::string BILL_TIME_FORMAT = "ddd MMMM d yyyy hh:mm";

struct Sidechain {
    uint8_t nSidechain;
    CScript depositScript;

    std::string ToString() const;
    std::string GetSidechainName() const;
};

//! Withdrawal status / zone (unspent, included in a bundle, paid out)
static const char WITHDRAWAL_UNSPENT = 'u';
static const char WITHDRAWAL_IN_BUNDLE = 'p';
static const char WITHDRAWAL_SPENT = 's';

//! Withdrawal Bundle status / zone
static const char WITHDRAWAL_BUNDLE_CREATED = 'c';
static const char WITHDRAWAL_BUNDLE_FAILED = 'f';
static const char WITHDRAWAL_BUNDLE_SPENT = 'o';

//! Key ID for fee script
static const std::string feeKey = "5f8f196a4f0c212fee1b4eda31e3ef383c52d9fc";
// 19iGcwHuZA1edpd6veLfbkHtbDPS9hAXbh
// ed7565854e9b7a334e39e33614abce078a6c06603b048a9536a7e41abf3da504

//! The default payment amount to mainchain miner for critical data commitment
static const CAmount DEFAULT_CRITICAL_DATA_AMOUNT = 0.0001 * COIN;

//! The fee for sidechain deposits on this sidechain
static const CAmount SIDECHAIN_DEPOSIT_FEE = 0.00001 * COIN;

static const char DB_SIDECHAIN_DEPOSIT_OP = 'D';
static const char DB_SIDECHAIN_WITHDRAWAL_OP = 'W';
static const char DB_SIDECHAIN_WITHDRAWAL_BUNDLE_OP = 'P';

/**
 * Base object for sidechain related database entries
 */
struct SidechainObj {
    char sidechainop;

    SidechainObj(void) { }
    virtual ~SidechainObj(void) { }

    uint256 GetHash(void) const;
    CScript GetScript(void) const;
    virtual std::string ToString(void) const;
};

/**
 * Sidechain individual withdrawal database object
 */
struct SidechainWithdrawal: public SidechainObj {
    uint8_t nSidechain;
    std::string strDestination;
    std::string strRefundDestination;
    CAmount amount;
    CAmount mainchainFee;
    char status;
    uint256 hashBlindTx; // Hash of transaction minus the serialization output

    SidechainWithdrawal(void) : SidechainObj() { sidechainop = DB_SIDECHAIN_WITHDRAWAL_OP; status = WITHDRAWAL_UNSPENT; }
    virtual ~SidechainWithdrawal(void) { }

    ADD_SERIALIZE_METHODS

    template <typename Stream, typename Operation>
    inline void SerializationOp(Stream& s, Operation ser_action) {
        READWRITE(sidechainop);
        READWRITE(nSidechain);
        READWRITE(strDestination);
        READWRITE(strRefundDestination);
        READWRITE(amount);
        READWRITE(mainchainFee);
        READWRITE(status);
        READWRITE(hashBlindTx);
    }

    std::string ToString(void) const;
    std::string GetStatusStr(void) const;

    uint256 GetID() const {
        SidechainWithdrawal withdrawal(*this);
        withdrawal.status = WITHDRAWAL_UNSPENT;
        return withdrawal.GetHash();
    }
};

/**
 * Sidechain withdrawal bundle proposal database object
 */
struct SidechainWithdrawalBundle: public SidechainObj {
    uint8_t nSidechain;
    CMutableTransaction tx;
    std::vector<uint256> vWithdrawalID; // The id in ldb of bundle's withdrawals
    int nHeight;
    // If the bundle fails we keep track of the sidechain height that it was
    // marked failed at so that we can wait WITHDRAWAL_BUNDLE_FAIL_WAIT_PERIOD
    // before trying the next bundle.
    int nFailHeight;
    char status;

    SidechainWithdrawalBundle(void) : SidechainObj() { sidechainop = DB_SIDECHAIN_WITHDRAWAL_BUNDLE_OP; status = WITHDRAWAL_BUNDLE_CREATED; nHeight = 0;}
    virtual ~SidechainWithdrawalBundle(void) { }

    ADD_SERIALIZE_METHODS

    template <typename Stream, typename Operation>
    inline void SerializationOp(Stream& s, Operation ser_action) {
        READWRITE(sidechainop);
        READWRITE(nSidechain);
        READWRITE(tx);
        READWRITE(vWithdrawalID);
        READWRITE(status);
        READWRITE(nHeight);
        READWRITE(nFailHeight);
    }

    uint256 GetID() const {
        SidechainWithdrawalBundle bundle(*this);
        bundle.status = WITHDRAWAL_BUNDLE_CREATED;
        bundle.nHeight = 0;
        bundle.nFailHeight = 0;
        return bundle.GetHash();
    }

    std::string ToString(void) const;

    std::string GetStatusStr(void) const;
};

/**
 * Sidechain deposit database object
 */
struct SidechainDeposit : public SidechainObj {
    uint8_t nSidechain;
    std::string strDest;
    CAmount amtUserPayout;
    CMutableTransaction dtx; // Mainchain deposit transaction
    uint32_t nBurnIndex; // Deposit burn output index
    uint32_t nTx; // Deposit transaction number in mainchain block
    uint256 hashMainchainBlock;

    SidechainDeposit(void) : SidechainObj() { sidechainop = DB_SIDECHAIN_DEPOSIT_OP; }
    virtual ~SidechainDeposit(void) { }

    SidechainDeposit(const SidechainDeposit* d) {
        sidechainop = d->sidechainop;
        nSidechain = d->nSidechain;
        strDest = d->strDest;
        amtUserPayout = d->amtUserPayout;
        dtx = d->dtx;
        nBurnIndex = d->nBurnIndex;
        nTx = d->nTx;
        hashMainchainBlock = d->hashMainchainBlock;
    }

    ADD_SERIALIZE_METHODS

    template <typename Stream, typename Operation>
    inline void SerializationOp(Stream& s, Operation ser_action) {
        READWRITE(sidechainop);
        READWRITE(nSidechain);
        READWRITE(strDest);
        READWRITE(amtUserPayout);
        READWRITE(dtx);
        READWRITE(nBurnIndex);
        READWRITE(nTx);
        READWRITE(hashMainchainBlock);
    }

    std::string ToString(void) const;

    bool operator==(const SidechainDeposit& d) const
    {
        if (sidechainop == d.sidechainop &&
                nSidechain == d.nSidechain &&
                strDest == d.strDest &&
                amtUserPayout == d.amtUserPayout &&
                dtx == d.dtx &&
                nBurnIndex == d.nBurnIndex &&
                nTx == d.nTx &&
                hashMainchainBlock == d.hashMainchainBlock) {
            return true;
        }
        return false;
    }

    uint256 GetID() const
    {
        SidechainDeposit deposit(*this);
        deposit.amtUserPayout = CAmount(0);
        return deposit.GetHash();
    }
};

// TODO move into a zside specific header
struct ScheduledZOperation
{
    // TODO upgrade Qt dependency version to use epoch time
    // The date and time that the command will be executed (unix time)
    // int nTime;
    std::string strTime;
    // The RPC command to be executed (z_sendmany ...)
    std::string strCommand;

    ADD_SERIALIZE_METHODS

    template<typename Stream, typename Operation>
    inline void SerializationOp(Stream& s, Operation ser_action) {
        // TODO READWRITE(nTime);
        READWRITE(strTime);
        READWRITE(strCommand);
    }

    std::string ToString(void) const;
};

/**
 * Parse sidechain object from a sidechain object script
 */
SidechainObj* ParseSidechainObj(const std::vector<unsigned char>& vch);

// Functions for both withdrawal bundle creation and the GUI to use in order to
// make sure that what the GUI displays (on the pending table) is the same
// as what the bundle creation code will actually select.

// Sort a vector of SidechainWithdrawal by mainchain fee in descending order
void SortWithdrawalByFee(std::vector<SidechainWithdrawal>& vWithdrawal);

// Sort a vector of SidechainWithdrawalBundle by height in descending order
void SortWithdrawalBundleByHeight(std::vector<SidechainWithdrawalBundle>& vWithdrawalBundle);

// Erase all SidechainWithdrawal from a vector which do not have WITHDRAWAL_UNSPENT status
void SelectUnspentWithdrawal(std::vector<SidechainWithdrawal>& vWithdrawal);

std::string GenerateDepositAddress(const std::string& strDestIn);

bool ParseDepositAddress(const std::string& strAddressIn, std::string& strAddressOut, unsigned int& nSidechainOut);

// TODO move bills into seperate header

// The cast/melt table uses fixed bill amounts for casting (deshielding).
//
// Vector of all bills and their broadcast day sorted from smallest to largest
// bill amount. This table is a list of powers of 2 from
// 0.0000,0001 to 21,990.2325,5552.
// Of course the table doesn't need to be in a container because the values can
// be easily calculated but I think that it will make things much easier to
// understand.
//
// All cast transactions will have to choose one of these bills, and broacast
// them on the day of the bill.
//
// As there is a shield fee of 0.0001, the bills below that amount have
// no chance of being used with current fee rules.

enum days
{
    BILL_MONDAY = 1,
    BILL_TUESDAY,
    BILL_WEDNESDAY,
    BILL_THURSDAY,
    BILL_FRIDAY,
    BILL_SATURDAY,
    BILL_SUNDAY,
};

struct Bill
{
    Bill(CAmount amountIn = CAmount(0), int nDayIn = 0)
    {
        amount = amountIn;
        nDay = nDayIn;
    };

    std::string GetDayStr() const {
        if (nDay == BILL_MONDAY)
            return "Monday";
        else
        if (nDay == BILL_TUESDAY)
            return "Tuesday";
        else
        if (nDay == BILL_WEDNESDAY)
            return "Wednesday";
        else
        if (nDay == BILL_THURSDAY)
            return "Thursday";
        else
        if (nDay == BILL_FRIDAY)
            return "Friday";
        else
        if (nDay == BILL_SATURDAY)
            return "Saturday";
        else
        if (nDay == BILL_SUNDAY)
            return "Sunday";
        else
            return "";
    }

    CAmount amount;
    int nDay;
};

static const std::vector<Bill> vBill =
{
    // Tier 6
    Bill(CAmount(0.00000001 * COIN), BILL_SATURDAY),
    Bill(CAmount(0.00000002 * COIN), BILL_FRIDAY),
    Bill(CAmount(0.00000004 * COIN), BILL_THURSDAY),
    Bill(CAmount(0.00000008 * COIN), BILL_WEDNESDAY),
    Bill(CAmount(0.00000016 * COIN), BILL_TUESDAY),
    Bill(CAmount(0.00000032 * COIN), BILL_MONDAY),
    Bill(CAmount(0.00000064 * COIN), BILL_SUNDAY),

    // Tier 5
    Bill(CAmount(0.00000128 * COIN), BILL_SATURDAY),
    Bill(CAmount(0.00000256 * COIN), BILL_FRIDAY),
    Bill(CAmount(0.00000512 * COIN), BILL_THURSDAY),
    Bill(CAmount(0.00001024 * COIN), BILL_WEDNESDAY),
    Bill(CAmount(0.00002048 * COIN), BILL_TUESDAY),
    Bill(CAmount(0.00004096 * COIN), BILL_MONDAY),
    Bill(CAmount(0.00008192 * COIN), BILL_SUNDAY),

    // Tier 4
    Bill(CAmount(0.00016384 * COIN), BILL_SATURDAY),
    Bill(CAmount(0.00032768 * COIN), BILL_FRIDAY),
    Bill(CAmount(0.00065536 * COIN), BILL_THURSDAY),
    Bill(CAmount(0.00131072 * COIN), BILL_WEDNESDAY),
    Bill(CAmount(0.00262144 * COIN), BILL_TUESDAY),
    Bill(CAmount(0.00524288 * COIN), BILL_MONDAY),
    Bill(CAmount(0.01048576 * COIN), BILL_SUNDAY),

    // Tier 3
    Bill(CAmount(0.02097152 * COIN), BILL_SATURDAY),
    Bill(CAmount(0.04194304 * COIN), BILL_FRIDAY),
    Bill(CAmount(0.08388608 * COIN), BILL_THURSDAY),
    Bill(CAmount(0.16777216 * COIN), BILL_WEDNESDAY),
    Bill(CAmount(0.33554432 * COIN), BILL_TUESDAY),
    Bill(CAmount(0.67108864 * COIN), BILL_MONDAY),
    Bill(CAmount(1.34217728 * COIN), BILL_SUNDAY),

    // Tier 2
    Bill(CAmount(2.68435456   * COIN), BILL_SATURDAY),
    Bill(CAmount(5.36870912   * COIN), BILL_FRIDAY),
    Bill(CAmount(10.73741824  * COIN), BILL_THURSDAY),
    Bill(CAmount(21.47483648  * COIN), BILL_WEDNESDAY),
    Bill(CAmount(42.94967296  * COIN), BILL_TUESDAY),
    Bill(CAmount(85.89934592  * COIN), BILL_MONDAY),
    Bill(CAmount(171.79869184 * COIN), BILL_SUNDAY),

    // Tier 1
    Bill(CAmount(343.59738368   * COIN), BILL_SATURDAY),
    Bill(CAmount(687.19476736   * COIN), BILL_FRIDAY),
    Bill(CAmount(1374.38953472  * COIN), BILL_THURSDAY),
    Bill(CAmount(2748.77906944  * COIN), BILL_WEDNESDAY),
    Bill(CAmount(5497.55813888  * COIN), BILL_TUESDAY),
    Bill(CAmount(10995.11627776 * COIN), BILL_MONDAY),
    Bill(CAmount(21990.23255552 * COIN), BILL_SUNDAY)
};

#endif // BITCOIN_PRIMITIVES_SIDECHAIN_H
