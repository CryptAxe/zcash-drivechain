// Copyright (c) 2021 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <qt/billtabledialog.h>
#include <qt/forms/ui_billtabledialog.h>

#include <QColor>
#include <QScrollBar>

#include <qt/bitcoinunits.h>

#include <amount.h>
#include <sidechain.h>

BillTableDialog::BillTableDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BillTableDialog)
{
    ui->setupUi(this);

    // Table style

    ui->tableWidgetBills->setColumnCount(4);
    ui->tableWidgetBills->setHorizontalHeaderLabels(QStringList() << "Bill Broadcast Day" << "" << "Bill Amounts" << "");
    ui->tableWidgetBills->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);

    // Resize cells (in a backwards compatible way)
#if QT_VERSION < 0x050000
    ui->tableWidgetBills->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
#else
    ui->tableWidgetBills->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
#endif

    ui->tableWidgetBills->verticalHeader()->setVisible(false);

    // Highlight single row
    // Select entire row
    ui->tableWidgetBills->setSelectionBehavior(QAbstractItemView::SelectRows);
    // Select only one row
    ui->tableWidgetBills->setSelectionMode(QAbstractItemView::SingleSelection);

    ShowBills();
}

BillTableDialog::~BillTableDialog()
{
    delete ui;
}

void BillTableDialog::ShowBills()
{
    ui->tableWidgetBills->setRowCount(14);

    // Add Bills to the table
    int nRow = 0;
    int nColumn = 1;
    bool fAddDays = true;
    for (const Bill& b : vBill) {

        // We only want to add days to the first column
        if (fAddDays) {
            // Bill Day
            QString strDay = QString::fromStdString(b.GetDayStr());
            QTableWidgetItem *itemDay = new QTableWidgetItem();
            itemDay->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            itemDay->setText(strDay);
            itemDay->setFlags(itemDay->flags() & ~Qt::ItemIsEditable);
            ui->tableWidgetBills->setItem(nRow, COLUMN_DAY, itemDay);
        }

        // Bill Amount
        QString strAmount = BitcoinUnits::formatWithZUnit(BitcoinUnits::BTC, b.amount, false, BitcoinUnits::separatorNever);
        QTableWidgetItem *itemAmount = new QTableWidgetItem();
        itemAmount->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        itemAmount->setText(strAmount);
        itemAmount->setFlags(itemAmount->flags() & ~Qt::ItemIsEditable);
        ui->tableWidgetBills->setItem(nRow, nColumn, itemAmount);

        // Highlight the Bill amount green
        ui->tableWidgetBills->item(nRow, nColumn)->setForeground(QColor(64, 177, 52));

        nRow++;

        // Once we reach 14 rows, move on to the next column
        if (nRow % 14 == 0) {
            nRow = 0;
            nColumn++;
            fAddDays = false;
        }
    }
}
