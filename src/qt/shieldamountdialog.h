// Copyright (c) 2021 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_SHIELDAMOUNTDIALOG_H
#define BITCOIN_SHIELDAMOUNTDIALOG_H

#include <QDialog>

#include <amount.h>

enum {
    MODE_SHIELD = 0,
    MODE_DESHIELD,
};

namespace Ui {
class ShieldAmountDialog;
}

class ShieldAmountDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ShieldAmountDialog(QWidget *parent = nullptr);
    ~ShieldAmountDialog();

    void setAmount(const CAmount& amount);
    CAmount getAmount() const;
    void setMode(int nMode);

public Q_SLOTS:
    void on_pushButtonContinue_clicked();

private:
    Ui::ShieldAmountDialog *ui;
    CAmount amountIn;
    CAmount amountOut;
    int nMode;
};

#endif // BITCOIN_SHIELDAMOUNTDIALOG_H
