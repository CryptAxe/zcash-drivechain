// Copyright (c) 2021 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_ZPAGE_H
#define BITCOIN_ZPAGE_H

#include <QWidget>

#include <amount.h>

class ClientModel;
class PlatformStyle;
class WalletModel;

namespace Ui {
class ZPage;
}

QT_BEGIN_NAMESPACE
class QSignalMapper;
QT_END_NAMESPACE

enum
{
    COLUMN_SHIELD_BUTTON = 0,
    COLUMN_SHIELD_AMOUNT,
    COLUMN_SHIELD_ADDRESS,
    COLUMN_SHIELD_DATE,
    COLUMN_SHIELD_CONFIRMATIONS,
};

enum
{
    COLUMN_DESHIELD_BUTTON = 0,
    COLUMN_DESHIELD_AMOUNT,
    COLUMN_DESHIELD_ADDRESS,
};

class ZPage : public QWidget
{
    Q_OBJECT

public:
    explicit ZPage(const PlatformStyle *platformStyle, QWidget *parent = nullptr);
    ~ZPage();

    void setClientModel(ClientModel *model);
    void setWalletModel(WalletModel *model);

public Q_SLOTS:
    void updateDisplayUnit();
    void setBalance(const CAmount& balance, const CAmount& unconfirmedBalance,
        const CAmount& immatureBalance, const CAmount& watchOnlyBalance,
        const CAmount& watchUnconfBalance, const CAmount& watchImmatureBalance);
    void setNumBlocks(const int nBlocks);
    void shieldButtonPushed(const int nRow);
    void deshieldButtonPushed(const int nRow);
    void updateTables();
    void on_pushButtonGetStatus_clicked();
    void on_pushButtonHelpZAddr_clicked();

private:
    Ui::ZPage *ui;

    void UpdateZBalance();
    bool GetNewTAddr(std::string& strAddress);
    void LoadZAddress();

    QSignalMapper *shieldSignalMapper = nullptr;
    QSignalMapper *deshieldSignalMapper = nullptr;

    WalletModel *walletModel = nullptr;
    ClientModel *clientModel = nullptr;
    const PlatformStyle *platformStyle;

    int nDisplayUnit;
    CAmount tBalance;
    CAmount zBalance;
    std::string zAddr = "";

};

#endif // BITCOIN_ZPAGE_H
