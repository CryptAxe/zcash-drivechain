// Copyright (c) 2021 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_MELTCASTHELPDIALOG_H
#define BITCOIN_MELTCASTHELPDIALOG_H

#include <QDialog>

namespace Ui {
class MeltCastHelpDialog;
}

class MeltCastHelpDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MeltCastHelpDialog(QWidget *parent = nullptr);
    ~MeltCastHelpDialog();

public Q_SLOTS:
    void on_pushButtonBills_clicked();

private:
    Ui::MeltCastHelpDialog *ui;
};

#endif // BITCOIN_MELTCASTHELPDIALOG_H
