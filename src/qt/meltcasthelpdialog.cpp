// Copyright (c) 2021 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <qt/meltcasthelpdialog.h>
#include <qt/forms/ui_meltcasthelpdialog.h>

#include <qt/billtabledialog.h>

MeltCastHelpDialog::MeltCastHelpDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MeltCastHelpDialog)
{
    ui->setupUi(this);
}

MeltCastHelpDialog::~MeltCastHelpDialog()
{
    delete ui;
}

void MeltCastHelpDialog::on_pushButtonBills_clicked()
{
    BillTableDialog dialog;
    dialog.exec();
}
