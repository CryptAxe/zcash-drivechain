// Copyright (c) 2021 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <qt/zpage.h>
#include <qt/forms/ui_zpage.h>

#include <QMessageBox>
#include <QSignalMapper>

#include <qt/amountfield.h>
#include <qt/addresstablemodel.h>
#include <qt/bitcoinunits.h>
#include <qt/guiutil.h>
#include <qt/clientmodel.h>
#include <qt/optionsmodel.h>
#include <qt/platformstyle.h>
#include <qt/shieldamountdialog.h>
#include <qt/rpcconsole.h>
#include <qt/walletmodel.h>

#include <wallet/wallet.h>

#include <rpc/server.h>
#include <asyncrpcqueue.h>

ZPage::ZPage(const PlatformStyle *_platformStyle, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ZPage),
    platformStyle(_platformStyle)
{
    ui->setupUi(this);

    shieldSignalMapper = new QSignalMapper(this);
    connect(shieldSignalMapper, SIGNAL(mapped(int)), this, SLOT(shieldButtonPushed(int)));

    deshieldSignalMapper = new QSignalMapper(this);
    connect(deshieldSignalMapper, SIGNAL(mapped(int)), this, SLOT(deshieldButtonPushed(int)));

    // shield table

    ui->tableWidgetShield->setColumnCount(COLUMN_SHIELD_CONFIRMATIONS + 1);
    ui->tableWidgetShield->setHorizontalHeaderLabels(
                QStringList() << "" << "Amount" << "Address"
                << "Date" << "# Conf");
    ui->tableWidgetShield->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);

    // Resize cells (in a backwards compatible way)
#if QT_VERSION < 0x050000
    ui->tableWidgetShield->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
#else
    ui->tableWidgetShield->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
#endif

    ui->tableWidgetShield->horizontalHeader()->setStretchLastSection(false);
    ui->tableWidgetShield->verticalHeader()->setVisible(false);

    // deshield table

    ui->tableWidgetDeshield->setColumnCount(COLUMN_DESHIELD_ADDRESS + 1);
    ui->tableWidgetDeshield->setHorizontalHeaderLabels(
                QStringList() << "" << "Amount" << "Address");
    ui->tableWidgetDeshield->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);

    // Resize cells (in a backwards compatible way)
#if QT_VERSION < 0x050000
    ui->tableWidgetDeshield->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
#else
    ui->tableWidgetDeshield->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
#endif

    ui->tableWidgetDeshield->horizontalHeader()->setStretchLastSection(false);
    ui->tableWidgetDeshield->verticalHeader()->setVisible(false);

    // Highlight single row
    // Select entire row
    ui->tableWidgetShield->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidgetDeshield->setSelectionBehavior(QAbstractItemView::SelectRows);
    // Select only one row
    ui->tableWidgetShield->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableWidgetDeshield->setSelectionMode(QAbstractItemView::SingleSelection);

    // Disable sorting as it will confuse the signal mapper
    ui->tableWidgetShield->setSortingEnabled(false);
    ui->tableWidgetDeshield->setSortingEnabled(false);

    ui->pushButtonHelpZAddr->setIcon(platformStyle->SingleColorIcon(":/icons/transaction_0"));
}

ZPage::~ZPage()
{
    delete ui;
}

void ZPage::setClientModel(ClientModel *model)
{
    this->clientModel = model;
    if (model)
    {
        connect(model, SIGNAL(numBlocksChanged(int, QDateTime, double, bool)),
                this, SLOT(setNumBlocks(int)));

        setNumBlocks(model->getNumBlocks());
    }
}

void ZPage::setWalletModel(WalletModel *model)
{
    this->walletModel = model;
    if (model && model->getOptionsModel())
    {
        connect(model, SIGNAL(balanceChanged(CAmount,CAmount,CAmount,CAmount,CAmount,CAmount)), this,
                SLOT(setBalance(CAmount,CAmount,CAmount,CAmount,CAmount,CAmount)));

        connect(model->getOptionsModel(), SIGNAL(displayUnitChanged(int)), this, SLOT(updateDisplayUnit()));

        updateDisplayUnit();
        updateTables();
        LoadZAddress();
    }
}

void ZPage::setBalance(const CAmount& _balance, const CAmount& unconfirmedBalance,
                               const CAmount& immatureBalance, const CAmount& watchOnlyBalance,
                               const CAmount& watchUnconfBalance, const CAmount& watchImmatureBalance)
{
    tBalance = _balance;
    ui->labelTBalance->setText("t-address balance: " + BitcoinUnits::formatWithUnit(nDisplayUnit, tBalance));

    UpdateZBalance();
}

void ZPage::setNumBlocks(const int nBlocksIn)
{
    if (!clientModel)
        return;

    updateTables();
}

void ZPage::updateDisplayUnit()
{
    if (!walletModel)
        return;

    nDisplayUnit = walletModel->getOptionsModel()->getDisplayUnit();

    UpdateZBalance();
}

void ZPage::shieldButtonPushed(const int nRow)
{
    if (nRow >= ui->tableWidgetShield->rowCount())
        return;

    // Get the output / T Addr for which the shield button was clicked
    QTableWidgetItem* itemTAddr = ui->tableWidgetShield->item(nRow, COLUMN_SHIELD_ADDRESS);
    QString tAddr = itemTAddr->text();

    // Parse amount from table object
    CAmount amount;
    QTableWidgetItem* itemAmount = ui->tableWidgetShield->item(nRow, COLUMN_SHIELD_AMOUNT);
    QString qAmount = itemAmount->text();

    QMessageBox messageBox;

    if (!BitcoinUnits::parse(nDisplayUnit, qAmount, &amount)) {
        // This error message shouldn't ever actually be displayed - but
        // if parsing the amount from the table does fail we want to know.
        messageBox.setWindowTitle("Failed to parse output amount!");
        QString str = QString("<p>Failed to parse output amount!</p>");
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }

    // Check that output is greater than shield fee
    if (amount <= CAmount(0.0001 * COIN)) {
        messageBox.setWindowTitle("Output amount is too small!");
        QString str = QString("This output is too small to pay the shield fee (0.0001)\n");
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }

    // Ask how much they want to shield
    CAmount amountToShield = CAmount(0);
    ShieldAmountDialog amountDialog;
    amountDialog.setMode(MODE_SHIELD);
    amountDialog.setAmount(amount);
    amountDialog.exec();
    amountToShield = amountDialog.getAmount();

    if (amountToShield == CAmount(0))
        return;

    // Subtract fee
    amountToShield -= CAmount(0.0001 * COIN);

    // re-format now that we have selected the amount and subracted fee
    qAmount = BitcoinUnits::format(nDisplayUnit, amountToShield, false, BitcoinUnits::separatorNever);

    // TODO update z_sendmany to ignore commas
    // z_sendmany amounts are decimal amounts with up to 8 digits
    // and the comma will count as 1 so we remove it.
    qAmount.remove(QChar(','), Qt::CaseInsensitive);

    if (!tAddr.size()) {
        // This error message shouldn't be displayed but we don't want to
        // ignore the error if it happens.
        messageBox.setWindowTitle("Invalid from T Address!");
        QString str = QString("<p>Failed to parse T Address!</p>");
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }
    if (!zAddr.size()) {
        // This error message shouldn't be displayed but we don't want to
        // ignore the error if it happens.
        messageBox.setWindowTitle("No Z Address found!");
        QString str = QString("<p>You must have a Z addres to shield transactions!</p>");
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }

    // Confirm that the user wants to create shield request and warn about fee
    QMessageBox confMessage;
    confMessage.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    confMessage.setDefaultButton(QMessageBox::Cancel);
    confMessage.setIcon(QMessageBox::Question);
    confMessage.setWindowTitle("Confirm shield request");
    QString strConf = "You are going to shield: \n";
    strConf += BitcoinUnits::formatWithUnit(nDisplayUnit, amountToShield) + "\n\n";
    strConf += "From your T Address:\n" + tAddr + "\n\n";
    strConf += "To your Z Address:\n" + QString::fromStdString(zAddr) + "\n\n";
    strConf += "0.0001 is deducted deducted for shield fee!\n\n";
    strConf += "Are you sure?\n";
    confMessage.setText(strConf);

    int nRes = confMessage.exec();
    if (nRes == QMessageBox::Cancel)
        return;

    // Format RPC command
    std::string strCommand = "z_sendmany";
    strCommand += " \"";
    strCommand += tAddr.toStdString();
    strCommand += "\" ";
    strCommand += "\'[{\"address\": ";
    strCommand += "\"";
    strCommand += zAddr;
    strCommand += "\"";
    strCommand += ", \"amount\": ";
    strCommand += qAmount.toStdString();
    strCommand += "}]\'";

    std::string strResult = "";
    std::string strError = "";
    bool fError = false;
    try {
        RPCConsole::RPCExecuteCommandLine(strResult, strCommand);
    } catch (std::exception& e) {
        fError = true;
        LogPrintf("%s: Error executing shield RPC command. Error: %s.\n", __func__, e.what());
    } catch (...) {
        fError = true;
        LogPrintf("%s: Error executing shield RPC command. Unknown exception.\n", __func__);
    }

    if (fError) {
        messageBox.setWindowTitle("Error creating shield operation!");
        QString str = "Failed to create shield operation!";
        if (!strError.empty())
            str += "Error: " + QString::fromStdString(strError) + "\n";
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }

    messageBox.setWindowTitle("Your shield operation has been queued!");
    QString str = "Please check the output of the RPC command \"z_getoperationstatus\"";
    str += " to view status.\n\n";
    str += "Shielding " + qAmount + " to " + QString::fromStdString(zAddr) + "\n\n";
    str += "Operation ID:\n" + QString::fromStdString(strResult);
    messageBox.setText(str);
    messageBox.setIcon(QMessageBox::Information);
    messageBox.setStandardButtons(QMessageBox::Ok);
    messageBox.exec();
}

void ZPage::deshieldButtonPushed(const int nRow)
{
    if (!walletModel || !walletModel->getOptionsModel())
        return;

    if (nRow >= ui->tableWidgetDeshield->rowCount())
        return;

    QMessageBox messageBox;

    // Parse amount from table object
    CAmount amount;
    QTableWidgetItem* item = ui->tableWidgetDeshield->item(nRow, COLUMN_DESHIELD_AMOUNT);
    QString qAmount = item->text();

    if (!BitcoinUnits::parse(nDisplayUnit, qAmount, &amount)) {
        // This error message shouldn't ever actually be displayed - but
        // if parsing the amount from the table does fail we want to know.
        messageBox.setWindowTitle("Failed to parse bill amount!");
        QString str = QString("<p>Failed to parse bill amount!</p>");
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }

    // Check that the amount is greater than the deshield fee
    if (amount <= 0.0001 * COIN) {
        messageBox.setWindowTitle("Output amount is too small!");
        QString str = QString("This output is too small to pay the deshield fee (0.0001)\n");
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }

    // Ask how much they want to deshield
    CAmount amountToDeshield = CAmount(0);
    ShieldAmountDialog amountDialog;
    amountDialog.setMode(MODE_DESHIELD);
    amountDialog.setAmount(amount);
    amountDialog.exec();
    amountToDeshield = amountDialog.getAmount();

    if (amountToDeshield == CAmount(0))
        return;

    // Subtract fee
    amountToDeshield -= CAmount(0.0001 * COIN);

    // re-format now that we have selected the amount and subracted fee
    qAmount = BitcoinUnits::format(nDisplayUnit, amountToDeshield, false, BitcoinUnits::separatorNever);

    // z_sendmany amounts are decimal amounts with up to 8 digits
    // and the comma will count as 1 so we remove it.
    qAmount.remove(QChar(','), Qt::CaseInsensitive);

    if (!zAddr.size()) {
        // This error message shouldn't be displayed but we don't want to
        // ignore the error if it happens.
        messageBox.setWindowTitle("Invalid from Z Address!");
        QString str = QString("<p>Failed to parse Z Address!</p>");
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }

    // Get a new t Addr
    std::string strTAddr = "";
    if (!GetNewTAddr(strTAddr)) {
        messageBox.setWindowTitle("Failed to generate new T Address!");
        QString str = QString("Failed to generated new T Address!\n");
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }

    // Confirm that the user wants to create deshield request and warn about fee
    QMessageBox confMessage;
    confMessage.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    confMessage.setDefaultButton(QMessageBox::Cancel);
    confMessage.setIcon(QMessageBox::Question);
    confMessage.setWindowTitle("Confirm deshield request");
    QString strConf = "You are going to deshield: \n";
    strConf += BitcoinUnits::formatWithUnit(nDisplayUnit, amountToDeshield) + "\n\n";
    strConf += "From your Z Address:\n" + QString::fromStdString(zAddr) + "\n\n";
    strConf += "To your T Address:\n" + QString::fromStdString(strTAddr) + "\n\n";
    strConf += "0.0001 is deducted for deshield fee!\n\n";
    strConf += "Are you sure?\n";
    confMessage.setText(strConf);

    int nRes = confMessage.exec();
    if (nRes == QMessageBox::Cancel)
        return;

    // Format RPC command
    std::string strCommand = "z_sendmany";
    strCommand += " \"";
    strCommand += zAddr;
    strCommand += "\" ";
    strCommand += "\'[{\"address\": ";
    strCommand += "\"";
    strCommand += strTAddr;
    strCommand += "\"";
    strCommand += ", \"amount\": ";
    strCommand += qAmount.toStdString();
    strCommand += "}]\'";

    std::string strResult = "";
    std::string strError = "";
    bool fError = false;
    try {
        RPCConsole::RPCExecuteCommandLine(strResult, strCommand);
    } catch (std::exception& e) {
        fError = true;
        LogPrintf("%s: Error executing deshield RPC command. Error: %s.\n", __func__, e.what());
    } catch (...) {
        fError = true;
        LogPrintf("%s: Error executing deshield RPC command. Unknown exception.\n", __func__);
    }

    if (fError) {
        messageBox.setWindowTitle("Error creating deshield operation!");
        QString str = "Failed to create deshield operation!";
        if (!strError.empty())
            str += "Error: " + QString::fromStdString(strError) + "\n";
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }

    messageBox.setWindowTitle("Your deshield operation has been queued!");
    QString str = "Please check the output of the RPC command \"z_getoperationstatus\"";
    str += " to view status.\n\n";
    str += "Deshielding " + qAmount + " to " + QString::fromStdString(strTAddr) + "\n\n";
    str += "Operation ID:\n" + QString::fromStdString(strResult);
    messageBox.setText(str);
    messageBox.setIcon(QMessageBox::Information);
    messageBox.setStandardButtons(QMessageBox::Ok);
    messageBox.exec();
}

void ZPage::updateTables()
{
    if (!walletModel || !walletModel->getOptionsModel()
            || !walletModel->getAddressTableModel())
        return;

    ui->tableWidgetShield->setRowCount(0);
    ui->tableWidgetDeshield->setRowCount(0);

    // Get the t address coins from the wallet
    std::map<QString, std::vector<COutput> > mapCoins;
    walletModel->listCoins(mapCoins);

    // Add normal coins to the shield table
    int nRow = 0;
    for (const std::pair<QString, std::vector<COutput>>& coins : mapCoins) {
        for (const COutput& out : coins.second) {
            ui->tableWidgetShield->insertRow(nRow);

            uint256 txhash = out.tx->GetHash();

            // Check if the coin is locked - if it is we will create the
            // item with disabled status
            bool fLocked = walletModel->isLockedCoin(txhash, out.i);

            // shield button
            QTableWidgetItem *itemButton = new QTableWidgetItem();
            itemButton->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            itemButton->setFlags(itemButton->flags() & ~Qt::ItemIsEditable);
            if (fLocked)
                itemButton->setFlags(itemButton->flags() & ~Qt::ItemIsEnabled);
            ui->tableWidgetShield->setItem(nRow, COLUMN_SHIELD_BUTTON, itemButton);

            QPushButton* shieldButton = new QPushButton(this);
            shieldButton->setText("Shield");
            ui->tableWidgetShield->setCellWidget(nRow, COLUMN_SHIELD_BUTTON, shieldButton);

            shieldSignalMapper->setMapping(shieldButton, nRow);
            connect(shieldButton, SIGNAL(clicked()), shieldSignalMapper, SLOT(map()));

            // Amount
            QString strAmount = BitcoinUnits::format(nDisplayUnit, out.tx->tx->vout[out.i].nValue, false, BitcoinUnits::separatorNever);
            QTableWidgetItem *itemAmount = new QTableWidgetItem();
            itemAmount->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            itemAmount->setText(strAmount);
            itemAmount->setFlags(itemAmount->flags() & ~Qt::ItemIsEditable);
            if (fLocked)
                itemAmount->setFlags(itemAmount->flags() & ~Qt::ItemIsEnabled);
            ui->tableWidgetShield->setItem(nRow, COLUMN_SHIELD_AMOUNT, itemAmount);

            // Address
            CTxDestination outputAddress;
            QString strAddress = "";
            if(ExtractDestination(out.tx->tx->vout[out.i].scriptPubKey, outputAddress))
            {
                strAddress = QString::fromStdString(EncodeDestination(outputAddress));
            }
            QTableWidgetItem *itemAddress = new QTableWidgetItem();
            itemAddress->setText(strAddress);
            itemAddress->setFlags(itemAddress->flags() & ~Qt::ItemIsEditable);
            if (fLocked)
                itemAddress->setFlags(itemAddress->flags() & ~Qt::ItemIsEnabled);
            ui->tableWidgetShield->setItem(nRow, COLUMN_SHIELD_ADDRESS, itemAddress);

            // Date
            QString strDate = GUIUtil::dateTimeStr(out.tx->GetTxTime());
            QTableWidgetItem *itemDate = new QTableWidgetItem();
            itemDate->setText(strDate);
            itemDate->setFlags(itemDate->flags() & ~Qt::ItemIsEditable);
            if (fLocked)
                itemDate->setFlags(itemDate->flags() & ~Qt::ItemIsEnabled);
            ui->tableWidgetShield->setItem(nRow, COLUMN_SHIELD_DATE, itemDate);

            // Confirmations
            QString strConf = QString::number(out.nDepth);
            QTableWidgetItem *itemConf = new QTableWidgetItem();
            itemConf->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            itemConf->setText(strConf);
            itemConf->setFlags(itemConf->flags() & ~Qt::ItemIsEditable);
            if (fLocked)
                itemConf->setFlags(itemConf->flags() & ~Qt::ItemIsEnabled);
            ui->tableWidgetShield->setItem(nRow, COLUMN_SHIELD_CONFIRMATIONS, itemConf);

            nRow++;
        }
    }

    // Get list of z-addresses from the wallet
    std::vector<std::string> vZAddr;
    {
        LOCK2(cs_main, vpwallets[0]->cs_wallet);
        std::set<libzcash::SaplingPaymentAddress> vAddr;
        vpwallets[0]->GetSaplingPaymentAddresses(vAddr);
        for (auto addr : vAddr) {
            if (HaveSpendingKeyForPaymentAddress(vpwallets[0])(addr)) {
                vZAddr.push_back(EncodePaymentAddress(addr));
            }
        }
    }

    nRow = 0;
    for (const std::string& s : vZAddr) {
        std::vector<SaplingNoteEntry> vNote;
        walletModel->listFilteredNotes(vNote, zAddr);

        for (const SaplingNoteEntry& note : vNote) {
            ui->tableWidgetDeshield->insertRow(nRow);

            // Deshield button
            QTableWidgetItem *itemButton = new QTableWidgetItem();
            itemButton->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            itemButton->setFlags(itemButton->flags() & ~Qt::ItemIsEditable);
            ui->tableWidgetDeshield->setItem(nRow, COLUMN_DESHIELD_BUTTON, itemButton);

            QPushButton* deshieldButton = new QPushButton(this);
            deshieldButton->setText("Deshield");
            ui->tableWidgetDeshield->setCellWidget(nRow, COLUMN_DESHIELD_BUTTON, deshieldButton);

            deshieldSignalMapper->setMapping(deshieldButton, nRow);
            connect(deshieldButton, SIGNAL(clicked()), deshieldSignalMapper, SLOT(map()));

            // Amount
            QTableWidgetItem *itemAmount = new QTableWidgetItem();
            itemAmount->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            itemAmount->setText(BitcoinUnits::format(nDisplayUnit, note.note.value(), false, BitcoinUnits::separatorNever));
            itemAmount->setFlags(itemAmount->flags() & ~Qt::ItemIsEditable);
            ui->tableWidgetDeshield->setItem(nRow, COLUMN_DESHIELD_AMOUNT, itemAmount);

            // Address
            QTableWidgetItem *itemAddr = new QTableWidgetItem();
            itemAddr->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            itemAddr->setText(QString::fromStdString(s));
            itemAddr->setFlags(itemAddr->flags() & ~Qt::ItemIsEditable);
            ui->tableWidgetDeshield->setItem(nRow, COLUMN_DESHIELD_ADDRESS, itemAddr);

            nRow++;
        }
    }
}

void ZPage::UpdateZBalance()
{
    if (!walletModel || !walletModel->getOptionsModel())
        return;

    CAmount zBalanceTotal = CAmount(0);
    std::vector<std::string> vZAddr;
    {
        LOCK2(cs_main, vpwallets[0]->cs_wallet);
        std::set<libzcash::SaplingPaymentAddress> vAddr;
        vpwallets[0]->GetSaplingPaymentAddresses(vAddr);
        for (auto addr : vAddr) {
            std::vector<SaplingNoteEntry> vNote;
            walletModel->listFilteredNotes(vNote, EncodePaymentAddress(addr));

            for (const SaplingNoteEntry& note : vNote)
                zBalanceTotal += note.note.value();
        }
    }

    int unit = walletModel->getOptionsModel()->getDisplayUnit();
    ui->labelZBalance->setText("z-address balance:  " + BitcoinUnits::formatWithZUnit(unit, zBalanceTotal, false, BitcoinUnits::separatorNever));

    zBalance = zBalanceTotal;

    updateTables();
}

bool ZPage::GetNewTAddr(std::string& strAddress)
{
    if (vpwallets.empty())
        return false;

    LOCK2(cs_main, vpwallets[0]->cs_wallet);
    EnsureWalletIsUnlocked(vpwallets[0]);

    if (!vpwallets[0]->IsLocked())
        vpwallets[0]->TopUpKeyPool();

    // Generate a new key that is added to wallet
    CPubKey newKey;
    if (!vpwallets[0]->GetKeyFromPool(newKey))
        return false;

    OutputType output_type = g_address_type;
    vpwallets[0]->LearnRelatedScripts(newKey, output_type);
    CTxDestination dest = GetDestinationForKey(newKey, output_type);
    vpwallets[0]->SetAddressBook(dest, "deshielded", "receive");

    strAddress = EncodeDestination(dest);

    return true;
}

void ZPage::LoadZAddress()
{
    if (vpwallets.empty())
        return;

    LOCK2(cs_main, vpwallets[0]->cs_wallet);
    EnsureWalletIsUnlocked(vpwallets[0]);

    // Load Z Address - create a new one if it hasn't been created yet.
    std::string strMasterZ = "";
    vpwallets[0]->ReadMasterZAddr(strMasterZ);

    if (strMasterZ.empty()) {
        // Generate new master Z address & save
        std::string strNewZ = EncodePaymentAddress(vpwallets[0]->GenerateNewSaplingZKey());
        strMasterZ = strNewZ;

        // Write
        vpwallets[0]->SetMasterZAddr(strMasterZ);
    }

    zAddr = strMasterZ;

    // Display Z Addr
    ui->labelZAddr->setText(QString::fromStdString(strMasterZ));
}

void ZPage::on_pushButtonGetStatus_clicked()
{
    std::shared_ptr<AsyncRPCQueue> pQ = getAsyncRPCQueue();
    std::vector<AsyncRPCOperationId> vID = pQ->getAllOperationIds();

    QString strStatus = "Operations:\n\n";

    for (auto id : vID) {
        std::shared_ptr<AsyncRPCOperation> pOp = pQ->getOperationForId(id);
        if (!pOp)
            continue;

        UniValue obj = pOp->getStatus();
        strStatus += QString::fromStdString(id) + "\nStatus: " + QString::fromStdString(obj["status"].get_str()) + "\n\n";
    }

    if (vID.empty())
        strStatus += "None right now.\n";

    QMessageBox messageBox;
    messageBox.setWindowTitle("ZCash operation status");
    messageBox.setText(strStatus);
    messageBox.setIcon(QMessageBox::Information);
    messageBox.setStandardButtons(QMessageBox::Ok);
    messageBox.exec();
}

void ZPage::on_pushButtonHelpZAddr_clicked()
{
    QMessageBox messageBox;
    messageBox.setWindowTitle("z-address information");
    QString str = "This z-address is saved by your wallet to be used for shield transactions.\n";
    str+= "z-addresses are fully private. It is therefore safe to re-use them.\n";
    messageBox.setText(str);
    messageBox.setIcon(QMessageBox::Information);
    messageBox.setStandardButtons(QMessageBox::Ok);
    messageBox.exec();
}
