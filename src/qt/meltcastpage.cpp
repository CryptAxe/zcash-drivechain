// Copyright (c) 2021 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <qt/meltcastpage.h>
#include <qt/forms/ui_meltcastpage.h>

#include <QColor>
#include <QDateTime>
#include <QMessageBox>
#include <QScrollBar>
#include <QSignalMapper>
#include <QTimer>

#include <qt/amountfield.h>
#include <qt/addresstablemodel.h>
#include <qt/bitcoinunits.h>
#include <qt/billtabledialog.h>
#include <qt/clientmodel.h>
#include <qt/guiutil.h>
#include <qt/meltcasthelpdialog.h>
#include <qt/optionsmodel.h>
#include <qt/platformstyle.h>
#include <qt/rpcconsole.h>
#include <qt/walletmodel.h>

#include <wallet/wallet.h>

#include <algorithm>
#include <stdlib.h>
#include <time.h>

MeltCastPage::MeltCastPage(const PlatformStyle *_platformStyle, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MeltCastPage),
    platformStyle(_platformStyle)
{
    ui->setupUi(this);

    meltSignalMapper = new QSignalMapper(this);
    connect(meltSignalMapper, SIGNAL(mapped(int)), this, SLOT(meltButtonPushed(int)));

    castSignalMapper = new QSignalMapper(this);
    connect(castSignalMapper, SIGNAL(mapped(int)), this, SLOT(castButtonPushed(int)));

    vScheduledZOperation.clear();
    LoadScheduledZOperations(vScheduledZOperation);
    UpdateNumScheduled();

    operationTimer = new QTimer(this);
    connect(operationTimer, SIGNAL(timeout()), this, SLOT(runScheduledOperations()));
    operationTimer->start(60 * 1000);

    utcUpdateTimer = new QTimer(this);
    connect(utcUpdateTimer, SIGNAL(timeout()), this, SLOT(updateTime()));
    utcUpdateTimer->start(950);

    // Melt table

    ui->tableWidgetMelt->setColumnCount(COLUMN_MELT_CONFIRMATIONS + 1);
    ui->tableWidgetMelt->setHorizontalHeaderLabels(
                QStringList() << "" << "Amount" << "Address"
                << "Date" << "# Conf");
    ui->tableWidgetMelt->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);

    // Resize cells (in a backwards compatible way)
#if QT_VERSION < 0x050000
    ui->tableWidgetMelt->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
#else
    ui->tableWidgetMelt->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
#endif

    ui->tableWidgetMelt->horizontalHeader()->setStretchLastSection(false);
    ui->tableWidgetMelt->verticalHeader()->setVisible(false);

    // Cast table

    ui->tableWidgetCast->setColumnCount(COLUMN_CAST_ETA + 1);
    ui->tableWidgetCast->setHorizontalHeaderLabels(
                QStringList() << "" << "Bill Amount" << "Broadcast Day" << "ETA");
    ui->tableWidgetCast->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);

    // Resize cells (in a backwards compatible way)
#if QT_VERSION < 0x050000
    ui->tableWidgetCast->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
#else
    ui->tableWidgetCast->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
#endif

    ui->tableWidgetCast->horizontalHeader()->setStretchLastSection(false);
    ui->tableWidgetCast->verticalHeader()->setVisible(false);

    // Highlight single row
    // Select entire row
    ui->tableWidgetMelt->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidgetCast->setSelectionBehavior(QAbstractItemView::SelectRows);
    // Select only one row
    ui->tableWidgetMelt->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableWidgetCast->setSelectionMode(QAbstractItemView::SingleSelection);

    // Disable sorting as it will confuse the signal mapper
    ui->tableWidgetMelt->setSortingEnabled(false);
    ui->tableWidgetCast->setSortingEnabled(false);

    // Disable vertical scroll on cast table
    ui->tableWidgetCast->verticalScrollBar()->setEnabled(false);
    ui->tableWidgetCast->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    // Buttons
    ui->pushButtonHelpZAddr->setIcon(platformStyle->SingleColorIcon(":/icons/transaction_0"));
    ui->pushButtonHelpCastsScheduled->setIcon(platformStyle->SingleColorIcon(":/icons/transaction_0"));
    ui->pushButtonHelp->setIcon(platformStyle->SingleColorIcon(":/icons/transaction_0"));
    ui->pushButtonCancelScheduled->setIcon(platformStyle->SingleColorIcon(":/icons/remove"));

    // Pixmap for melt & cast icons
    QIcon meltIcon = platformStyle->SingleColorIcon(":/icons/melt");
    ui->labelMeltIcon->setPixmap(meltIcon.pixmap(meltIcon.actualSize(QSize(32, 32))));
    QIcon castIcon = platformStyle->SingleColorIcon(":/icons/cast");
    ui->labelCastIcon->setPixmap(castIcon.pixmap(castIcon.actualSize(QSize(32, 32))));

    ui->labelBalance->setText("Available to melt: " + BitcoinUnits::formatWithUnit(BitcoinUnits::BTC, CAmount(0)));
    ui->labelZBalance->setText("Available to cast: " + BitcoinUnits::formatWithZUnit(BitcoinUnits::BTC, CAmount(0)));

    // Initialize bill map to help quickly lookup bill amounts to highlight
    for (const Bill& b : vBill)
        mapBill[b.amount] = b.nDay;

    // Event filter to catch mouse press event
    ui->frameMeltAll->installEventFilter(this);
    ui->frameCastAll->installEventFilter(this);

    // The user can pass command line / conf file arg --runscheduledoperations
    // to ignore operation scheduled time and run immediately.
    fRunScheduledNow = gArgs.GetBoolArg("-runscheduledoperations", false);

    updateTime();

    std::srand(std::time(NULL));
}

MeltCastPage::~MeltCastPage()
{
    DumpScheduledZOperations(vScheduledZOperation);
    delete ui;
}

bool MeltCastPage::eventFilter(QObject *obj, QEvent *event)
{
    // Catch click of melt all / cast all frame areas (treat as button)
    if (event->type() == QEvent::MouseButtonPress) {
        if (obj == ui->frameMeltAll) {
            MeltAll();
            return true;
        }
        else
        if (obj == ui->frameCastAll) {
            CastAll();
            return true;
        }
    }
    else  // Handle focus highlighting
    if (event->type() == QEvent::Enter) {
        if (obj == ui->frameMeltAll) {
            // Highlight with zcash orange
            ui->frameMeltAll->setStyleSheet("background-color: rgb(249, 187, 0);");
            // Change text color
            ui->labelMeltTitle->setStyleSheet("color: black;");
            ui->labelMeltSub->setStyleSheet("color: black;");
            return true;
        }
        else
        if (obj == ui->frameCastAll) {
            ui->frameCastAll->setStyleSheet("background-color: rgb(249, 187, 0);");
            ui->labelCastTitle->setStyleSheet("color: black;");
            ui->labelCastSub->setStyleSheet("color: black;");
            return true;
        }
    }
    else
    if (event->type() == QEvent::Leave) {
        if (obj == ui->frameMeltAll) {
            // Remove highlights and reset color
            ui->frameMeltAll->setStyleSheet("");
            ui->labelMeltTitle->setStyleSheet("");
            ui->labelMeltSub->setStyleSheet("");
            return true;
        }
        else
        if (obj == ui->frameCastAll) {
            ui->frameCastAll->setStyleSheet("");
            ui->labelCastTitle->setStyleSheet("");
            ui->labelCastSub->setStyleSheet("");
            return true;
        }
    }

    return QWidget::eventFilter(obj, event);
}

void MeltCastPage::setClientModel(ClientModel *model)
{
    this->clientModel = model;
    if (model)
    {
        connect(model, SIGNAL(numBlocksChanged(int, QDateTime, double, bool)),
                this, SLOT(setNumBlocks(int)));

        setNumBlocks(model->getNumBlocks());
    }
}

void MeltCastPage::setWalletModel(WalletModel *model)
{
    this->walletModel = model;
    if (model && model->getOptionsModel())
    {
        connect(model, SIGNAL(balanceChanged(CAmount,CAmount,CAmount,CAmount,CAmount,CAmount)), this,
                SLOT(setBalance(CAmount,CAmount,CAmount,CAmount,CAmount,CAmount)));

        connect(model->getOptionsModel(), SIGNAL(displayUnitChanged(int)), this, SLOT(updateDisplayUnit()));

        updateDisplayUnit();
        updateTables();
        LoadZAddress();
    }
}

void MeltCastPage::setNumBlocks(const int nBlocksIn)
{
    if (!clientModel)
        return;

    updateTables();
    UpdateZBalance();
}

void MeltCastPage::on_pushButtonHelp_clicked()
{
    MeltCastHelpDialog dialog;
    dialog.exec();
}

void MeltCastPage::on_pushButtonHelpCastsScheduled_clicked()
{
    QMessageBox messageBox;
    messageBox.setWindowTitle("Scheduled z operation information");

    QString str = QString("Scheduled Z Operations will run at the UTC time specified.\n");
    str += "Your ZSide node must be running at the scheduled time for the operation to complete!\n\n";

    // Add list of scheduled operations
    str += "Your currently scheduled operation times (UTC):\n\n";
    int i = 1;
    if (vScheduledZOperation.empty())
        str += "None scheduled.\n";
    for (const ScheduledZOperation& z : vScheduledZOperation) {
        str += QString::number(i) + " : " + QDateTime::fromString(QString::fromStdString(z.strTime), QString::fromStdString(BILL_TIME_FORMAT)).toString("ddd MMMM d yyyy hh:mm") + "\n";
        i++;
    }

    messageBox.setText(str);
    messageBox.setIcon(QMessageBox::Information);
    messageBox.setStandardButtons(QMessageBox::Ok);
    messageBox.exec();
}

void MeltCastPage::on_pushButtonHelpZAddr_clicked()
{
    QMessageBox messageBox;
    messageBox.setWindowTitle("z-address information");
    QString str = "This z-address is saved by your wallet to be used for all cast / melt transactions.\n";
    str+= "z-addresses are fully private. It is therefore safe to re-use them.\n";
    messageBox.setText(str);
    messageBox.setIcon(QMessageBox::Information);
    messageBox.setStandardButtons(QMessageBox::Ok);
    messageBox.exec();
}

void MeltCastPage::on_pushButtonCancelScheduled_clicked()
{
    // Confirm that the user wants to cancel scheduled operations
    QMessageBox confMessage;
    confMessage.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    confMessage.setDefaultButton(QMessageBox::Cancel);
    confMessage.setIcon(QMessageBox::Question);
    confMessage.setWindowTitle("Confirm scheduled operation reset!");
    QString strConf = "You are going to cancel all scheduled Z operations!\n\n";
    strConf += "Are you sure?\n";
    confMessage.setText(strConf);

    int nRes = confMessage.exec();
    if (nRes == QMessageBox::Cancel)
        return;

    // Erase
    vScheduledZOperation.clear();
    UpdateNumScheduled();
}

void MeltCastPage::on_pushButtonBills_clicked()
{
    BillTableDialog billDialog;
    billDialog.exec();

    // Also recolor the highlights (for testing)
    HighlightAmounts();
}

void MeltCastPage::on_tableWidgetMelt_doubleClicked(const QModelIndex& i)
{
    if (i.row() >= (int)vMeltDetailsCache.size())
        return;

    QString str = vMeltDetailsCache[i.row()];

    QMessageBox message;
    message.setStandardButtons(QMessageBox::Ok);
    message.setIcon(QMessageBox::Information);
    message.setWindowTitle("Melt Details");
    message.setText(str);

    message.exec();
}

void MeltCastPage::updateDisplayUnit()
{
    if (!walletModel)
        return;

    nDisplayUnit = walletModel->getOptionsModel()->getDisplayUnit();

    UpdateZBalance();
}

void MeltCastPage::meltButtonPushed(const int nRow)
{
    if (nRow >= ui->tableWidgetMelt->rowCount())
        return;

    // Get the output / T Addr for which the melt button was clicked
    QTableWidgetItem* itemTAddr = ui->tableWidgetMelt->item(nRow, COLUMN_MELT_ADDRESS);
    QString tAddr = itemTAddr->text();

    // Parse amount from table object
    CAmount amount;
    QTableWidgetItem* itemAmount = ui->tableWidgetMelt->item(nRow, COLUMN_MELT_AMOUNT);
    QString qAmount = itemAmount->text();

    QMessageBox messageBox;

    if (!BitcoinUnits::parse(nDisplayUnit, qAmount, &amount)) {
        // This error message shouldn't ever actually be displayed - but
        // if parsing the amount from the table does fail we want to know.
        messageBox.setWindowTitle("Failed to parse output amount!");
        QString str = QString("<p>Failed to parse output amount!</p>");
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }

    // Subtract shield fee (0.0001) from amount and check that it is still > 0
    amount -= CAmount(0.0001 * COIN);
    if (amount <= 0) {
        messageBox.setWindowTitle("Output amount is too small!");
        QString str = QString("This output is too small to pay the shield fee (0.0001)\n");
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }

    // re-format now that we have subtracted the fee
    qAmount = BitcoinUnits::format(nDisplayUnit, amount, false, BitcoinUnits::separatorNever);

    // TODO update z_sendmany to ignore commas
    // z_sendmany amounts are decimal amounts with up to 8 digits
    // and the comma will count as 1 so we remove it.
    qAmount.remove(QChar(','), Qt::CaseInsensitive);

    if (!tAddr.size()) {
        // This error message shouldn't be displayed but we don't want to
        // ignore the error if it happens.
        messageBox.setWindowTitle("Invalid from T Address!");
        QString str = QString("<p>Failed to parse T Address!</p>");
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }
    if (!zAddr.size()) {
        // This error message shouldn't be displayed but we don't want to
        // ignore the error if it happens.
        messageBox.setWindowTitle("No Z Address found!");
        QString str = QString("<p>You must have a Z addres to shield transactions!</p>");
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }

    // Confirm that the user wants to create shield request and warn about fee
    QMessageBox confMessage;
    confMessage.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    confMessage.setDefaultButton(QMessageBox::Cancel);
    confMessage.setIcon(QMessageBox::Question);
    confMessage.setWindowTitle("Confirm melt request");
    QString strConf = "You are going to shield: \n";
    strConf += BitcoinUnits::formatWithUnit(nDisplayUnit, amount) + "\n\n";
    strConf += "From your T Address:\n" + tAddr + "\n\n";
    strConf += "To your Z Address:\n" + QString::fromStdString(zAddr) + "\n\n";
    strConf += "0.0001 is deducted deducted for shield fee!\n\n";
    strConf += "Are you sure?\n";
    confMessage.setText(strConf);

    int nRes = confMessage.exec();
    if (nRes == QMessageBox::Cancel)
        return;

    // Format RPC command
    std::string strCommand = "z_sendmany";
    strCommand += " \"";
    strCommand += tAddr.toStdString();
    strCommand += "\" ";
    strCommand += "\'[{\"address\": ";
    strCommand += "\"";
    strCommand += zAddr;
    strCommand += "\"";
    strCommand += ", \"amount\": ";
    strCommand += qAmount.toStdString();
    strCommand += "}]\'";

    std::string strResult = "";
    std::string strError = "";
    bool fError = false;
    try {
        RPCConsole::RPCExecuteCommandLine(strResult, strCommand);
    } catch (std::exception& e) {
        fError = true;
        LogPrintf("%s: Error executing melt RPC command. Error: %s.\n", __func__, e.what());
    } catch (...) {
        fError = true;
        LogPrintf("%s: Error executing melt RPC command. Unknown exception.\n", __func__);
    }

    if (fError) {
        messageBox.setWindowTitle("Error creating shield operation!");
        QString str = "Failed to create shield operation!";
        if (!strError.empty())
            str += "Error: " + QString::fromStdString(strError) + "\n";
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }

    messageBox.setWindowTitle("Your shield operation has been queued!");
    QString str = "Please check the output of the RPC command \"z_getoperationstatus\"";
    str += " to view status.\n\n";
    str += "Shielding " + qAmount + " to " + QString::fromStdString(zAddr) + "\n\n";
    str += "Operation ID:\n" + QString::fromStdString(strResult);
    messageBox.setText(str);
    messageBox.setIcon(QMessageBox::Information);
    messageBox.setStandardButtons(QMessageBox::Ok);
    messageBox.exec();
}

void MeltCastPage::castButtonPushed(const int nRow)
{
    if (!walletModel || !walletModel->getOptionsModel())
        return;

    if (nRow >= ui->tableWidgetCast->rowCount())
        return;

    if (vScheduledZOperation.size()) {
        // Confirm that the user wants to create shield requests and warn about fees
        QMessageBox castsPendingMessage;
        castsPendingMessage.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        castsPendingMessage.setDefaultButton(QMessageBox::Cancel);
        castsPendingMessage.setIcon(QMessageBox::Warning);
        castsPendingMessage.setWindowTitle("Warning: scheduled operations still pending!");
        QString strConf = "You already have " + QString::number(vScheduledZOperation.size()) + " operation(s) scheduled.\n";
        strConf += "Your z-balance does not yet reflect the changes these scheduled operations will make.\n\n";
        strConf += "If your z-balance is not sufficient, scheduled operations will fail.\n\n";
        strConf += "Are you sure you want to schedule another?\n";
        castsPendingMessage.setText(strConf);

        int nRes = castsPendingMessage.exec();
        if (nRes == QMessageBox::Cancel)
            return;
    }

    QMessageBox messageBox;

    // Parse amount from table object
    CAmount amount;
    QTableWidgetItem* item = ui->tableWidgetCast->item(nRow, COLUMN_CAST_AMOUNT);
    QString qAmount = item->text();

    if (!BitcoinUnits::parse(nDisplayUnit, qAmount, &amount)) {
        // This error message shouldn't ever actually be displayed - but
        // if parsing the amount from the table does fail we want to know.
        messageBox.setWindowTitle("Failed to parse bill amount!");
        QString str = QString("<p>Failed to parse bill amount!</p>");
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }

    // Check that the amount is greater than the deshield fee
    if (amount <= 0.0001 * COIN) {
        messageBox.setWindowTitle("Output amount is too small!");
        QString str = QString("This output is too small to pay the deshield fee (0.0001)\n");
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }

    // TODO update z_sendmany to ignore commas
    // z_sendmany amounts are decimal amounts with up to 8 digits
    // and the comma will count as 1 so we remove it.
    qAmount.remove(QChar(','), Qt::CaseInsensitive);

    if (!zAddr.size()) {
        // This error message shouldn't be displayed but we don't want to
        // ignore the error if it happens.
        messageBox.setWindowTitle("Invalid from Z Address!");
        QString str = QString("<p>Failed to parse Z Address!</p>");
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }

    // Get a new t Addr
    std::string strTAddr = "";
    if (!GetNewTAddr(strTAddr)) {
        messageBox.setWindowTitle("Failed to generate new T Address!");
        QString str = QString("Failed to generated new T Address!\n");
        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Critical);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
        return;
    }

    // Get bill day from table
    QTableWidgetItem* itemDay = ui->tableWidgetCast->item(nRow, COLUMN_CAST_DAY);
    QString qDay = itemDay->text();
    int nBillDay = DayStringToInt(qDay.toStdString());

    // Confirm that the user wants to create deshield request and warn about fee
    QMessageBox confMessage;
    confMessage.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    confMessage.setDefaultButton(QMessageBox::Cancel);
    confMessage.setIcon(QMessageBox::Question);
    confMessage.setWindowTitle("Confirm cast request");
    QString strConf = "You are going to deshield: \n";
    strConf += BitcoinUnits::formatWithUnit(nDisplayUnit, amount) + "\n\n";
    strConf += "From your Z Address:\n" + QString::fromStdString(zAddr) + "\n\n";
    strConf += "To your T Address:\n" + QString::fromStdString(strTAddr) + "\n\n";
    strConf += "0.0001 is deducted for deshield fee!\n\n";
    strConf += "Are you sure?\n";
    confMessage.setText(strConf);

    int nRes = confMessage.exec();
    if (nRes == QMessageBox::Cancel)
        return;

    // Format RPC command
    std::string strCommand = "z_sendmany";
    strCommand += " \"";
    strCommand += zAddr;
    strCommand += "\" ";
    strCommand += "\'[{\"address\": ";
    strCommand += "\"";
    strCommand += strTAddr;
    strCommand += "\"";
    strCommand += ", \"amount\": ";
    strCommand += qAmount.toStdString();
    strCommand += "}]\'";

    QString dateString = "";

    ScheduledZOperation operation;
    operation.strCommand = strCommand;
    // TODO operation.nTime = GetBillUnixTime(nBillDay, dateString);
    GetBillUnixTime(nBillDay, dateString);
    operation.strTime = dateString.toStdString();

    ScheduleOperation(operation);

    messageBox.setWindowTitle("Your cast operation has been scheduled!");
    QString str = "Your cast has been scheduled for:\n";
    str += dateString += "\n";
    messageBox.setText(str);
    messageBox.setIcon(QMessageBox::Information);
    messageBox.setStandardButtons(QMessageBox::Ok);
    messageBox.exec();
}

void MeltCastPage::setBalance(const CAmount& _balance, const CAmount& unconfirmedBalance,
                               const CAmount& immatureBalance, const CAmount& watchOnlyBalance,
                               const CAmount& watchUnconfBalance, const CAmount& watchImmatureBalance)
{
    balance = _balance;
    ui->labelBalance->setText("Available to melt: " + BitcoinUnits::formatWithUnit(nDisplayUnit, balance));

    UpdateZBalance();
}

void MeltCastPage::updateTables()
{
    if (!walletModel || !walletModel->getOptionsModel()
            || !walletModel->getAddressTableModel())
        return;

    ui->tableWidgetMelt->setRowCount(0);
    ui->tableWidgetCast->setRowCount(0);

    vMeltDetailsCache.clear();

    // Get the normal non zcash coins from the wallet
    std::map<QString, std::vector<COutput> > mapCoins;
    walletModel->listCoins(mapCoins);

    // Add normal coins to the melt table
    int nRow = 0;
    for (const std::pair<QString, std::vector<COutput>>& coins : mapCoins) {
        for (const COutput& out : coins.second) {
            ui->tableWidgetMelt->insertRow(nRow);

            uint256 txhash = out.tx->GetHash();

            // Check if the coin is locked - if it is we will create the
            // item with disabled status
            bool fLocked = walletModel->isLockedCoin(txhash, out.i);

            // Melt button
            QTableWidgetItem *itemButton = new QTableWidgetItem();
            itemButton->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            itemButton->setFlags(itemButton->flags() & ~Qt::ItemIsEditable);
            if (fLocked)
                itemButton->setFlags(itemButton->flags() & ~Qt::ItemIsEnabled);
            ui->tableWidgetMelt->setItem(nRow, COLUMN_MELT_BUTTON, itemButton);

            QPushButton* meltButton = new QPushButton(this);
            meltButton->setText("Melt");
            ui->tableWidgetMelt->setCellWidget(nRow, COLUMN_MELT_BUTTON, meltButton);

            meltSignalMapper->setMapping(meltButton, nRow);
            connect(meltButton, SIGNAL(clicked()), meltSignalMapper, SLOT(map()));

            // Amount
            QString strAmount = BitcoinUnits::format(nDisplayUnit, out.tx->tx->vout[out.i].nValue, false, BitcoinUnits::separatorNever);
            QTableWidgetItem *itemAmount = new QTableWidgetItem();
            itemAmount->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            itemAmount->setText(strAmount);
            itemAmount->setFlags(itemAmount->flags() & ~Qt::ItemIsEditable);
            if (fLocked)
                itemAmount->setFlags(itemAmount->flags() & ~Qt::ItemIsEnabled);
            ui->tableWidgetMelt->setItem(nRow, COLUMN_MELT_AMOUNT, itemAmount);

            // Address
            CTxDestination outputAddress;
            QString strAddress = "";
            if(ExtractDestination(out.tx->tx->vout[out.i].scriptPubKey, outputAddress))
            {
                strAddress = QString::fromStdString(EncodeDestination(outputAddress));
            }
            QTableWidgetItem *itemAddress = new QTableWidgetItem();
            itemAddress->setText(strAddress);
            itemAddress->setFlags(itemAddress->flags() & ~Qt::ItemIsEditable);
            if (fLocked)
                itemAddress->setFlags(itemAddress->flags() & ~Qt::ItemIsEnabled);
            ui->tableWidgetMelt->setItem(nRow, COLUMN_MELT_ADDRESS, itemAddress);

            // Date
            QString strDate = GUIUtil::dateTimeStr(out.tx->GetTxTime());
            QTableWidgetItem *itemDate = new QTableWidgetItem();
            itemDate->setText(strDate);
            itemDate->setFlags(itemDate->flags() & ~Qt::ItemIsEditable);
            if (fLocked)
                itemDate->setFlags(itemDate->flags() & ~Qt::ItemIsEnabled);
            ui->tableWidgetMelt->setItem(nRow, COLUMN_MELT_DATE, itemDate);

            // Confirmations
            QString strConf = QString::number(out.nDepth);
            QTableWidgetItem *itemConf = new QTableWidgetItem();
            itemConf->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            itemConf->setText(strConf);
            itemConf->setFlags(itemConf->flags() & ~Qt::ItemIsEditable);
            if (fLocked)
                itemConf->setFlags(itemConf->flags() & ~Qt::ItemIsEnabled);
            ui->tableWidgetMelt->setItem(nRow, COLUMN_MELT_CONFIRMATIONS, itemConf);

            // Cache details of t-address output (melt)
            QString strDetails = "t-address output details:\n\n";
            strDetails += "Amount:\n";
            strDetails += strAmount + "\n\n";
            strDetails += "Address:\n";
            strDetails += strAddress + "\n\n";
            strDetails += "Date created:\n";
            strDetails += strDate + "\n\n";
            strDetails += "Confirmations:\n";
            strDetails += strConf + "\n\n";
            strDetails += "TxID:\n";
            strDetails += QString::fromStdString(txhash.ToString()) + "\n\n";
            strDetails += "n:\n";
            strDetails += QString::number(out.i) + "\n";
            vMeltDetailsCache.push_back(strDetails);

            nRow++;
        }
    }

    CAmount amountLeftToCast = zBalance;

    // Get bills for the amount requested
    if (amountLeftToCast <= zBalance) {
        // Calculate bills
        std::vector<Bill> vBill;

        // Get up to 4 bills
        for (int i = 0; i < 4; i++) {
            Bill bill;
            if (!GetBill(amountLeftToCast, bill))
                break;

            // Add bill to vector and subtract bill amount from amount to cast
            vBill.push_back(bill);
            amountLeftToCast -= bill.amount;
        }

        // Add bills to table
        nRow = 0;
        for (const Bill& b : vBill) {
            ui->tableWidgetCast->insertRow(nRow);

            // Cast button
            QTableWidgetItem *itemButton = new QTableWidgetItem();
            itemButton->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            itemButton->setFlags(itemButton->flags() & ~Qt::ItemIsEditable);
            ui->tableWidgetCast->setItem(nRow, COLUMN_CAST_BUTTON, itemButton);

            QPushButton* castButton = new QPushButton(this);
            castButton->setText("Cast");
            ui->tableWidgetCast->setCellWidget(nRow, COLUMN_CAST_BUTTON, castButton);

            castSignalMapper->setMapping(castButton, nRow);
            connect(castButton, SIGNAL(clicked()), castSignalMapper, SLOT(map()));

            // Amount
            QTableWidgetItem *itemAmount = new QTableWidgetItem();
            itemAmount->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            itemAmount->setText(BitcoinUnits::format(nDisplayUnit, b.amount, false, BitcoinUnits::separatorNever));
            itemAmount->setFlags(itemAmount->flags() & ~Qt::ItemIsEditable);
            ui->tableWidgetCast->setItem(nRow, COLUMN_CAST_AMOUNT, itemAmount);

            // Day
            QTableWidgetItem *itemDay = new QTableWidgetItem();
            itemDay->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            itemDay->setText(QString::fromStdString(b.GetDayStr()));
            itemDay->setFlags(itemAmount->flags() & ~Qt::ItemIsEditable);
            ui->tableWidgetCast->setItem(nRow, COLUMN_CAST_DAY, itemDay);

            // ETA
            QTableWidgetItem *itemETA = new QTableWidgetItem();
            itemETA->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            itemETA->setText(GetBillETA(b.nDay));
            itemETA->setFlags(itemAmount->flags() & ~Qt::ItemIsEditable);
            ui->tableWidgetCast->setItem(nRow, COLUMN_CAST_ETA, itemETA);

            nRow++;
        }
    }

    // Highlight t-address amounts based on whether they match bill amounts
    HighlightAmounts();
}

void MeltCastPage::runScheduledOperations()
{
    if (vScheduledZOperation.empty())
        return;

    // Do not try to run operations if z address balance is 0.
    // Another operation may have spent z address coins and now we
    // need to wait for the change.
    if (zBalance == CAmount(0))
        return;

    // Get current time and day of the week
    QDateTime currentDateTime = QDateTime::currentDateTimeUtc();
    int nCurrentDay = currentDateTime.date().dayOfWeek();

    QMessageBox messageBox;

    // Look for scheduled operations that should run now
    std::vector<ScheduledZOperation> vComplete;
    for (const ScheduledZOperation& z : vScheduledZOperation) {
        QDateTime billDateTime = QDateTime::fromString(QString::fromStdString(z.strTime), QString::fromStdString(BILL_TIME_FORMAT));

        // Check if it is time to run scheduled operation, skip if the user
        // asked to run them now.
        if (!fRunScheduledNow) {
            int nBillDay = billDateTime.date().dayOfWeek();

            // Skip if bill isn't for the current day
            if (nCurrentDay != nBillDay)
                continue;

            QTime currentTime = currentDateTime.time();
            QTime billTime = billDateTime.time();

            // For now, this code will execute operations with a sheduled UTC time
            // that are +/- 1 hour from the current UTC time

            // Check if bill time is +/- 1 hour from current time
            if (std::abs(currentTime.secsTo(billTime)) > 60 * 60)
                continue;
        }

        // Time to execute the operation
        std::string strResult = "";
        std::string strError = "";
        bool fError = false;
        try {
            RPCConsole::RPCExecuteCommandLine(strResult, z.strCommand);
        } catch (std::exception& e) {
            fError = true;
            LogPrintf("%s: Error executing cast RPC command. Error: %s.\n", __func__, e.what());
        } catch (...) {
            fError = true;
            LogPrintf("%s: Error executing cast RPC command. Unknown exception.\n", __func__);
        }

        if (fError) {
            messageBox.setWindowTitle("Error executing scheduled z operation!");
            QString str = "Failed to create cast operation!";
            if (!strError.empty())
                str += "Error: " + QString::fromStdString(strError) + "\n";
            messageBox.setText(str);
            messageBox.setIcon(QMessageBox::Critical);
            messageBox.setStandardButtons(QMessageBox::Ok);
            messageBox.exec();
        } else {
            vComplete.push_back(z);
            // If we complete one operation we don't want to run anymore yet
            // because the z-address utxos might have all been spent and
            // now we have to wait for the change to return.
            break;
        }
    }

    // TODO remove expired operations

    if (vComplete.size()) {
        // Display results
        messageBox.setWindowTitle("Completed scheduled operation!");
        QString str = "One of your scheduled operations has been executed!\n\n";
        str += "Please check the output of the RPC command \"z_getoperationresult\" to view more details.\n";

        messageBox.setText(str);
        messageBox.setIcon(QMessageBox::Information);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();

        // TODO check operation results before removing
        // Remove completed operations. Completion of RPC command does not
        // mean we're sure that the operation will ever go though though
        for (const ScheduledZOperation& z : vComplete)
            RemoveScheduledOperation(z);

        UpdateNumScheduled();
        updateTables();
        UpdateZBalance();
    }
}

void MeltCastPage::updateTime()
{
    QDateTime currentTime = QDateTime::currentDateTimeUtc();
    ui->labelTime->setText("Current UTC time: " + currentTime.toString("ddd MMM d hh:mm:ss"));
}

void MeltCastPage::UpdateZBalance()
{
    if (!walletModel || !walletModel->getOptionsModel())
        return;

    // Get sapling notes from the wallet for our z address
    std::vector<SaplingNoteEntry> vNote;
    walletModel->listFilteredNotes(vNote, zAddr);

    CAmount zBalanceTotal = CAmount(0);
    for (const SaplingNoteEntry& note : vNote) {
        zBalanceTotal += note.note.value();
    }

    int unit = walletModel->getOptionsModel()->getDisplayUnit();
    ui->labelZBalance->setText("Available to cast:  " + BitcoinUnits::formatWithZUnit(unit, zBalanceTotal, false, BitcoinUnits::separatorNever));

    zBalance = zBalanceTotal;

    updateTables();
}

void MeltCastPage::LoadZAddress()
{
    if (vpwallets.empty())
        return;

    LOCK2(cs_main, vpwallets[0]->cs_wallet);
    EnsureWalletIsUnlocked(vpwallets[0]);

    // Load Z Address - create a new one if it hasn't been created yet.
    std::string strMasterZ = "";
    vpwallets[0]->ReadMasterZAddr(strMasterZ);

    if (strMasterZ.empty()) {
        // Generate new master Z address & save
        std::string strNewZ = EncodePaymentAddress(vpwallets[0]->GenerateNewSaplingZKey());
        strMasterZ = strNewZ;

        // Write
        vpwallets[0]->SetMasterZAddr(strMasterZ);
    }

    // Display Z Addr
    zAddr = strMasterZ;
    ui->labelZAddr->setText(QString::fromStdString(strMasterZ));
}

bool MeltCastPage::GetNewTAddr(std::string& strAddress)
{
    if (vpwallets.empty())
        return false;

    LOCK2(cs_main, vpwallets[0]->cs_wallet);
    EnsureWalletIsUnlocked(vpwallets[0]);

    if (!vpwallets[0]->IsLocked())
        vpwallets[0]->TopUpKeyPool();

    // Generate a new key that is added to wallet
    CPubKey newKey;
    if (!vpwallets[0]->GetKeyFromPool(newKey))
        return false;

    OutputType output_type = g_address_type;
    vpwallets[0]->LearnRelatedScripts(newKey, output_type);
    CTxDestination dest = GetDestinationForKey(newKey, output_type);
    vpwallets[0]->SetAddressBook(dest, "casted", "receive");

    strAddress = EncodeDestination(dest);

    return true;
}

void MeltCastPage::UpdateNumScheduled()
{
    ui->labelNumCastsScheduled->setText(QString::number(vScheduledZOperation.size()));
}

void MeltCastPage::ScheduleOperation(const ScheduledZOperation& operation)
{
    // Check for duplicate
    for (const ScheduledZOperation& z : vScheduledZOperation) {
        if (z.strTime == operation.strTime && z.strCommand == operation.strCommand)
            return;
    }
    vScheduledZOperation.push_back(operation);
    UpdateNumScheduled();
}

void MeltCastPage::RemoveScheduledOperation(const ScheduledZOperation& operation)
{
    // Find operation and remove if found
    for (size_t i = 0; i < vScheduledZOperation.size(); i++) {
        if (vScheduledZOperation[i].strTime == operation.strTime &&
                vScheduledZOperation[i].strCommand == operation.strCommand) {
            vScheduledZOperation[i] = vScheduledZOperation.back();
            vScheduledZOperation.pop_back();
            break;
        }
    }
    UpdateNumScheduled();
}

void MeltCastPage::HighlightAmounts()
{
    if (!walletModel || !walletModel->getOptionsModel())
        return;

    int nRows = ui->tableWidgetMelt->rowCount();
    if (nRows == 0)
        return;

    // Look for t-address outputs that we should highlight
    for (int i = 0; i < nRows; i++) {
        // Parse amount from table object
        CAmount amount;
        QString qAmount = ui->tableWidgetMelt->item(i, COLUMN_MELT_AMOUNT)->text();
        if (!BitcoinUnits::parse(nDisplayUnit, qAmount, &amount)) {
            return;
        }

        // Is this amount a bill amount? Highlight red if not.
        bool fBillAmount = (mapBill.find(amount) != mapBill.end());
        if (!fBillAmount) {
            // We are going to generate a random shade of red within a certain
            // range.

            // Ranges of colors we wish to higlight with:
            int nRMin = 170;
            int nRMax = 255;
            int nGMin = 0;
            int nGMax = 40;
            int nBMin = 0;
            int nBMax = 47;

            int nR = std::rand() % (nRMax - nRMin + 1) + nRMin;
            int nG = std::rand() % (nGMax - nGMin + 1) + nGMin;
            int nB = std::rand() % (nBMax - nBMin + 1) + nBMin;

            // Apply as the background color for this amount cell
            ui->tableWidgetMelt->item(i, COLUMN_MELT_AMOUNT)->setBackground(QColor(nR, nG, nB));
        } else {
            // Highlight amount green if it matches a bill amount
            ui->tableWidgetMelt->item(i, COLUMN_MELT_AMOUNT)->setForeground(QColor(64, 177, 52));
        }
    }
}

int MeltCastPage::GetBillUnixTime(int nBillDay, QString& dateString)
{
    // Get the current time and day of the week
    QDateTime now = QDateTime::currentDateTimeUtc();
    int nCurrentDay = now.date().dayOfWeek();

    // Get distance from the current day to the next nBillDay
    int nDist = 0;
    int nDay = nCurrentDay;
    while (nDay != nBillDay) {
        nDist++;
        nDay == 7 ? nDay = 1 : nDay++;
    }

    QDateTime result = now.addDays(nDist);

    // Come up with random broadcast time
    int nHour = std::rand() % 23 + 1;
    int nMinute = std::rand() % 59 + 1;

    QTime broadcastTime(nHour, nMinute);
    result.setTime(broadcastTime);

    // Return result date string
    dateString = result.toString(QString::fromStdString(BILL_TIME_FORMAT));

    // TODO toSecsSinceEpoch instead of date string
    //return result.toSecsSinceEpoch();

    return 0;
}

QString MeltCastPage::GetBillETA(int nBillDay)
{
    // Get the current time and day of the week
    QDateTime now = QDateTime::currentDateTimeUtc();
    int nCurrentDay = now.date().dayOfWeek();

    // Get distance from the current day to the next nBillDay
    int nDist = 0;
    int nDay = nCurrentDay;
    while (nDay != nBillDay) {
        nDist++;
        nDay == 7 ? nDay = 1 : nDay++;
    }

    if (nDist == 0) {
        return "Today";
    }
    else if (nDist == 1) {
        return "Tomorrow";
    } else {
        return QString::number(nDist) + " Days";
    }
}

void MeltCastPage::MeltAll()
{
    if (!walletModel || !walletModel->getOptionsModel())
        return;

    int nRows = ui->tableWidgetMelt->rowCount();
    if (nRows == 0)
        return;

    QMessageBox messageBox;

    // Confirm that the user wants to create shield requests and warn about fees
    QMessageBox confMessage;
    confMessage.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    confMessage.setDefaultButton(QMessageBox::Cancel);
    confMessage.setIcon(QMessageBox::Question);
    confMessage.setWindowTitle("Confirm melt all request");
    QString strConf = "You are going to shield all of your t-address coins!\n\n";
    strConf += "All shieldable outputs will be sent to your Z Address:\n" + QString::fromStdString(zAddr) + "\n\n";
    strConf += "0.0001 will be deducted from each output for shield fee!\n\n";
    strConf += "Are you sure?\n";
    confMessage.setText(strConf);

    int nRes = confMessage.exec();
    if (nRes == QMessageBox::Cancel)
        return;

    // Vector of results or error message
    std::vector<std::string> vError;

    // Try to shield each output
    CAmount amountTotal = CAmount(0);
    for (int i = 0; i < nRows; i++) {
        // Get T Addr
        QString tAddr = ui->tableWidgetMelt->item(i, COLUMN_MELT_ADDRESS)->text();

        if (!tAddr.size()) {
            // This error message shouldn't be displayed but we don't want to
            // ignore the error if it happens.
            messageBox.setWindowTitle("Invalid from T Address!");
            QString str = QString("<p>Failed to parse T Address!</p>");
            messageBox.setText(str);
            messageBox.setIcon(QMessageBox::Critical);
            messageBox.setStandardButtons(QMessageBox::Ok);
            messageBox.exec();
            return;
        }

        // Parse amount from table object
        CAmount amount;
        QString qAmount = ui->tableWidgetMelt->item(i, COLUMN_MELT_AMOUNT)->text();
        if (!BitcoinUnits::parse(nDisplayUnit, qAmount, &amount)) {
            // This error message shouldn't ever actually be displayed - but
            // if parsing the amount from the table does fail we want to know.
            messageBox.setWindowTitle("Failed to parse output amount!");
            QString str = QString("<p>Failed to parse output amount!</p>");
            messageBox.setText(str);
            messageBox.setIcon(QMessageBox::Critical);
            messageBox.setStandardButtons(QMessageBox::Ok);
            messageBox.exec();
            return;
        }

        std::string strOutput = qAmount.toStdString() + " : " + tAddr.toStdString() + "\n";

        // Subtract shield fee (0.0001) from amount and check that is still > 0
        amount -= CAmount(0.0001 * COIN);
        if (amount <= 0) {
            // Record error to results and continue
            std::string strFail = "Error: Amount too small! : " + strOutput;
            vError.push_back(strFail);
            continue;
        }

        // re-format now that we have subtracted the fee
        qAmount = BitcoinUnits::format(nDisplayUnit, amount, false, BitcoinUnits::separatorNever);

        // TODO update z_sendmany to ignore commas
        // z_sendmany amounts are decimal amounts with up to 8 digits
        // and the comma will count as 1 so we remove it.
        qAmount.remove(QChar(','), Qt::CaseInsensitive);

        // Format RPC command
        std::string strCommand = "z_sendmany";
        strCommand += " \"";
        strCommand += tAddr.toStdString();
        strCommand += "\" ";
        strCommand += "\'[{\"address\": ";
        strCommand += "\"";
        strCommand += zAddr;
        strCommand += "\"";
        strCommand += ", \"amount\": ";
        strCommand += qAmount.toStdString();
        strCommand += "}]\'";

        std::string strResult = "";
        std::string strError = "";
        bool fError = false;
        try {
            RPCConsole::RPCExecuteCommandLine(strResult, strCommand);
        } catch (std::exception& e) {
            fError = true;
            LogPrintf("%s: Error executing melt RPC command. Error: %s.\n", __func__, e.what());
        } catch (...) {
            fError = true;
            LogPrintf("%s: Error executing melt RPC command. Unknown exception.\n", __func__);
        }

        if (fError) {
            std::string strFail = "";
            if (!strError.empty())
                strFail += "Error: " + strError + " : ";
            else
                strFail += "Error : ";
            strFail += strOutput;
            vError.push_back(strFail);
            continue;
        }

        // Track total amount shielded
        amountTotal += amount;
    }

    // Display results - tell them to check z_getoperationstatus
    messageBox.setWindowTitle("Your shield operations have been queued!");
    QString str = "Please check the output of the RPC command \"z_getoperationstatus\" to view status.\n\n";
    str += "Shielding " + BitcoinUnits::format(nDisplayUnit, amountTotal) + " to " + QString::fromStdString(zAddr) + "\n\n";
    if (vError.size())
        str += "Errors:\n";
    for (const std::string& s : vError)
        str += QString::fromStdString(s);
    messageBox.setText(str);
    messageBox.setIcon(QMessageBox::Information);
    messageBox.setStandardButtons(QMessageBox::Ok);
    messageBox.exec();
}

void MeltCastPage::CastAll()
{
    if (!walletModel || !walletModel->getOptionsModel())
        return;

    int nRows = ui->tableWidgetCast->rowCount();
    if (nRows == 0)
        return;

    if (vScheduledZOperation.size()) {
        // Confirm that the user wants to create shield requests and warn about fees
        QMessageBox castsPendingMessage;
        castsPendingMessage.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        castsPendingMessage.setDefaultButton(QMessageBox::Cancel);
        castsPendingMessage.setIcon(QMessageBox::Warning);
        castsPendingMessage.setWindowTitle("Warning: scheduled operations still pending!");
        QString strConf = "You currently have " + QString::number(vScheduledZOperation.size()) + " operation(s) scheduled.\n";
        strConf += "Your z-balance does not yet reflect the changes these scheduled operations will make.\n\n";
        strConf += "If your z-balance is not sufficient, scheduled operations will fail.\n\n";
        strConf += "Are you sure you want to schedule another?\n";
        castsPendingMessage.setText(strConf);

        int nRes = castsPendingMessage.exec();
        if (nRes == QMessageBox::Cancel)
            return;
    }

    QMessageBox messageBox;

    // Confirm that the user wants to create shield requests and warn about fees
    QMessageBox confMessage;
    confMessage.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    confMessage.setDefaultButton(QMessageBox::Cancel);
    confMessage.setIcon(QMessageBox::Question);
    confMessage.setWindowTitle("Confirm cast all request");
    QString strConf = "You are going to deshield all of your z-address coins!\n\n";
    strConf += "This will move 95-100% of your z-address balance into a maximum of 4 new transparent outputs depending on z-balance.\n\n";
    strConf += "0.0001 will be deducted from each output for deshield fee!\n\n";
    strConf += "Are you sure?\n";
    confMessage.setText(strConf);

    int nRes = confMessage.exec();
    if (nRes == QMessageBox::Cancel)
        return;

    // Vector of results or error message
    std::vector<std::string> vError;

    // Try to deshield each output
    CAmount amountTotal = CAmount(0);
    for (int i = 0; i < nRows; i++) {
        // Parse amount from table object
        CAmount amount;
        QString qAmount = ui->tableWidgetCast->item(i, COLUMN_CAST_AMOUNT)->text();
        if (!BitcoinUnits::parse(nDisplayUnit, qAmount, &amount)) {
            // This error message shouldn't ever actually be displayed - but
            // if parsing the amount from the table does fail we want to know.
            messageBox.setWindowTitle("Failed to parse output amount!");
            QString str = QString("<p>Failed to parse output amount!</p>");
            messageBox.setText(str);
            messageBox.setIcon(QMessageBox::Critical);
            messageBox.setStandardButtons(QMessageBox::Ok);
            messageBox.exec();
            return;
        }

        std::string strOutput = qAmount.toStdString() + " : " + zAddr + "\n";

        // Check that the amount is greater than the deshield fee
        if (amount <= 0.0001 * COIN) {
            // Record error to results and continue
            std::string strFail = "Error: Amount too small! : " + strOutput;
            vError.push_back(strFail);
            continue;
        }

        amountTotal += amount;

        // TODO update z_sendmany to ignore commas
        // z_sendmany amounts are decimal amounts with up to 8 digits
        // and the comma will count as 1 so we remove it.
        qAmount.remove(QChar(','), Qt::CaseInsensitive);

        if (!zAddr.size()) {
            // This error message shouldn't be displayed but we don't want to
            // ignore the error if it happens.
            messageBox.setWindowTitle("Invalid from Z Address!");
            QString str = QString("<p>Failed to parse Z Address!</p>");
            messageBox.setText(str);
            messageBox.setIcon(QMessageBox::Critical);
            messageBox.setStandardButtons(QMessageBox::Ok);
            messageBox.exec();
            return;
        }

        // Get a new t Addr
        std::string strTAddr = "";
        if (!GetNewTAddr(strTAddr)) {
            messageBox.setWindowTitle("Failed to generate new T Address!");
            QString str = QString("Failed to generated new T Address!\n");
            messageBox.setText(str);
            messageBox.setIcon(QMessageBox::Critical);
            messageBox.setStandardButtons(QMessageBox::Ok);
            messageBox.exec();
            return;
        }

        // Get bill day from table
        QTableWidgetItem* itemDay = ui->tableWidgetCast->item(i, COLUMN_CAST_DAY);
        QString qDay = itemDay->text();
        int nBillDay = DayStringToInt(qDay.toStdString());

        // Format RPC command
        std::string strCommand = "z_sendmany";
        strCommand += " \"";
        strCommand += zAddr;
        strCommand += "\" ";
        strCommand += "\'[{\"address\": ";
        strCommand += "\"";
        strCommand += strTAddr;
        strCommand += "\"";
        strCommand += ", \"amount\": ";
        strCommand += qAmount.toStdString();
        strCommand += "}]\'";

        QString dateString = "";

        ScheduledZOperation operation;
        operation.strCommand = strCommand;
        // TODO operation.nTime = GetBillUnixTime(nBillDay, dateString);
        GetBillUnixTime(nBillDay, dateString);
        operation.strTime = dateString.toStdString();

        ScheduleOperation(operation);
    }

    // Display results - tell them to check z_getoperationstatus
    messageBox.setWindowTitle("Your deshield operations have been queued!");
    QString str = "You can view your scheduled operations on the Melt / Cast page.\n";
    str += "Make sure that your zside node is running at the scheduled operation time(s)!\n\n";
    str += "Deshielding " + BitcoinUnits::format(nDisplayUnit, amountTotal) + "\n\n";
    if (vError.size())
        str += "Errors:\n";
    for (const std::string& s : vError)
        str += QString::fromStdString(s);
    messageBox.setText(str);
    messageBox.setIcon(QMessageBox::Information);
    messageBox.setStandardButtons(QMessageBox::Ok);
    messageBox.exec();
}

int DayStringToInt(const std::string strDay)
{
    if (strDay == "Monday")
        return 1;
    else
    if (strDay == "Tuesday")
        return 2;
    else
    if (strDay == "Wednesday")
        return 3;
    else
    if (strDay == "Thursday")
        return 4;
    else
    if (strDay == "Friday")
        return 5;
    else
    if (strDay == "Saturday")
        return 6;
    else
    if (strDay == "Sunday")
        return 7;
    else
        return 1;
}

bool GetBill(const CAmount& amountIn, Bill& billOut)
{
    // Amounts less than the shield / deshield fee will not work
    if (amountIn <= CAmount(0.0001 * COIN))
        return false;

    // Special case for amounts greater than or equal to largest bill amount
    if (amountIn >= vBill.back().amount) {
        billOut = vBill.back();
        return true;
    }

    // Return the Bill closest to but not greater than amount

    // Find the std::upper_bound
    auto upper = std::upper_bound(vBill.begin(), vBill.end(), amountIn,
                                  [](CAmount amount, const Bill& bill) {
                                      return amount < bill.amount;
                                  });

    if (upper == vBill.end() || upper == vBill.begin())
        return false;

    // Preceding bill is the largest bill that isn't greater than the amount
    billOut = *(upper - 1);

    // Bill amount less than shield / deshield fee will not work
    if (billOut.amount <= CAmount(0.0001 * COIN))
        return false;

    return true;
}
