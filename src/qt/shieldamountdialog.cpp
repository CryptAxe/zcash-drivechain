// Copyright (c) 2021 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <qt/shieldamountdialog.h>
#include <qt/forms/ui_shieldamountdialog.h>

#include <qt/amountfield.h>
#include <qt/bitcoinunits.h>

#include <QMessageBox>

ShieldAmountDialog::ShieldAmountDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ShieldAmountDialog)
{
    ui->setupUi(this);
    nMode = MODE_SHIELD;
    amountIn = CAmount(0);
    amountOut = CAmount(0);
}

ShieldAmountDialog::~ShieldAmountDialog()
{
    delete ui;
}

void ShieldAmountDialog::setAmount(const CAmount& amount)
{
    amountIn = amount;
    QString qAmount = BitcoinUnits::format(BitcoinUnits::BTC, amount, false, BitcoinUnits::separatorNever);
    QString str = nMode == MODE_SHIELD ? "Available to shield: " : "Available to deshield: ";
    ui->labelAmount->setText(str + qAmount);
    ui->lineEditAmount->setValue(amountIn);
}

CAmount ShieldAmountDialog::getAmount() const
{
    return amountOut;
}

void ShieldAmountDialog::setMode(int nModeIn)
{
    if (nModeIn == MODE_SHIELD) {
        nMode = MODE_SHIELD;
        ui->labelTitle->setText("Enter the amount you would like to shield.\n");
        ui->labelFee->setText("The shield of 0.0001 will be deducted from this amount.\n");
    }
    else
    if (nModeIn == MODE_DESHIELD) {
        nMode = MODE_DESHIELD;
        ui->labelTitle->setText("Enter the amount you would like to deshield.\n");
        ui->labelFee->setText("The deshield of 0.0001 will be deducted from this amount.\n");
    }
}

void ShieldAmountDialog::on_pushButtonContinue_clicked()
{
    ui->lineEditAmount->validate();
    CAmount amountSelected = ui->lineEditAmount->value();

    QMessageBox messageBox;
    messageBox.setIcon(QMessageBox::Critical);
    messageBox.setWindowTitle("Error - the amount you have entered is invalid");

    if (amountSelected == CAmount(0)) {
        messageBox.setText("The amount you have entered is invalid.\n");
        messageBox.exec();
        return;
    }

    if (amountSelected <= CAmount(0.0001 * COIN)) {
        messageBox.setText("Amount must be greater than shield / desheild fee (0.0001).\n");
        messageBox.exec();
        ui->lineEditAmount->setValid(false);
        return;
    }

    if (amountSelected > amountIn) {
        messageBox.setText("Amount cannot be greater than balance available.\n");
        messageBox.exec();
        ui->lineEditAmount->setValid(false);
        return;
    }

    amountOut = amountSelected;
    ui->lineEditAmount->setValid(true);
    this->close();
}
