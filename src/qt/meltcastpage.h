// Copyright (c) 2021 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

// TODO
// After upgrading Qt dependency to > 5.8 all datetimestring stuff
// should be removed and replaced with epoch time in seconds.
// The bill broadcast time should be stored as a number and not string.

#ifndef BITCOIN_MELTCASTPAGE_H
#define BITCOIN_MELTCASTPAGE_H

#include <QWidget>

#include <QMouseEvent>

#include <amount.h>

#include <map>
#include <vector>

class ClientModel;
class PlatformStyle;
class WalletModel;

class ScheduledZOperation;
class Bill;

QT_BEGIN_NAMESPACE
class QSignalMapper;
class QString;
class QTimer;
QT_END_NAMESPACE

enum
{
    COLUMN_MELT_BUTTON = 0,
    COLUMN_MELT_AMOUNT,
    COLUMN_MELT_ADDRESS,
    COLUMN_MELT_DATE,
    COLUMN_MELT_CONFIRMATIONS,
};

enum
{
    COLUMN_CAST_BUTTON = 0,
    COLUMN_CAST_AMOUNT,
    COLUMN_CAST_DAY,
    COLUMN_CAST_ETA,
};

int DayStringToInt(const std::string strDay);

// Return the bill closest to but not greater than amountIn
bool GetBill(const CAmount& amountIn, Bill& billOut);

namespace Ui {
class MeltCastPage;
}

class MeltCastPage : public QWidget
{
    Q_OBJECT

public:
    explicit MeltCastPage(const PlatformStyle *platformStyle, QWidget *parent = nullptr);
    ~MeltCastPage();

    void setClientModel(ClientModel *model);
    void setWalletModel(WalletModel *model);

public Q_SLOTS:
    void setBalance(const CAmount& balance, const CAmount& unconfirmedBalance,
                const CAmount& immatureBalance, const CAmount& watchOnlyBalance,
                const CAmount& watchUnconfBalance, const CAmount& watchImmatureBalance);
    void setNumBlocks(const int nBlocks);
    void on_pushButtonHelpCastsScheduled_clicked();
    void on_pushButtonHelpZAddr_clicked();
    void on_pushButtonHelp_clicked();
    void on_pushButtonCancelScheduled_clicked();
    void on_pushButtonBills_clicked();
    void on_tableWidgetMelt_doubleClicked(const QModelIndex& index);

private Q_SLOTS:
    void updateDisplayUnit();
    void meltButtonPushed(const int nRow);
    void castButtonPushed(const int nRow);
    void updateTables();
    void runScheduledOperations();
    void updateTime();

private:
    Ui::MeltCastPage *ui;

    void UpdateZBalance();
    void LoadZAddress();
    bool GetNewTAddr(std::string& strAddress);
    void UpdateNumScheduled();
    void ScheduleOperation(const ScheduledZOperation& operation);
    void RemoveScheduledOperation(const ScheduledZOperation& operation);
    void HighlightAmounts();

    // Return the unix time of the next nBillDay from current time
    int GetBillUnixTime(int nBillDay, QString& dateString);
    QString GetBillETA(int nBillDay);

    WalletModel *walletModel = nullptr;
    ClientModel *clientModel = nullptr;
    const PlatformStyle *platformStyle;

    QSignalMapper *meltSignalMapper = nullptr;
    QSignalMapper *castSignalMapper = nullptr;

    CAmount balance;
    CAmount zBalance;
    std::string zAddr = "";
    int nDisplayUnit;
    bool fRunScheduledNow;

    QTimer *operationTimer = nullptr;
    QTimer *utcUpdateTimer = nullptr;

    std::vector<ScheduledZOperation> vScheduledZOperation;
    std::map<CAmount, int> mapBill;

    std::vector<QString> vMeltDetailsCache;

    bool eventFilter(QObject* obj, QEvent *event);

    void MeltAll();
    void CastAll();
};

#endif // BITCOIN_MELTCASTPAGE_H
