// Copyright (c) 2020 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <qt/scheduledtransactiontablemodel.h>

#include <QTimer>

#include <qt/bitcoinunits.h>
#include <qt/clientmodel.h>
#include <qt/guiconstants.h>
#include <qt/optionsmodel.h>
#include <qt/walletmodel.h>

#include <primitives/transaction.h>
#include <uint256.h>
#include <validation.h>

Q_DECLARE_METATYPE(ScheduledTransactionTableObject)

ScheduledTransactionTableModel::ScheduledTransactionTableModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    vScheduled.clear();
    LoadScheduledTransactions(vScheduled);

    pollTimer = new QTimer(this);
    connect(pollTimer, SIGNAL(timeout()), this, SLOT(BroadcastScheduled()));
    pollTimer->start(30 * 1000);

    ScheduledTransaction tx;
    tx.tx = CMutableTransaction();
    tx.amount = CAmount(100);

    ScheduleTransaction(tx);

    ScheduledTransaction tx2;
    tx2.tx = CMutableTransaction();
    tx2.amount=CAmount(50000);
    QDateTime now = QDateTime::currentDateTime();
    tx2.nTime = now.addSecs(90).toMSecsSinceEpoch();
    ScheduleTransaction(tx2);
}

ScheduledTransactionTableModel::~ScheduledTransactionTableModel()
{
    DumpScheduledTransactions(vScheduled);
}

int ScheduledTransactionTableModel::rowCount(const QModelIndex & /*parent*/) const
{
    return model.size();
}

int ScheduledTransactionTableModel::columnCount(const QModelIndex & /*parent*/) const
{
    return 3;
}

QVariant ScheduledTransactionTableModel::data(const QModelIndex &index, int role) const
{
    if (!walletModel)
        return false;

    if (!index.isValid())
        return false;

    int row = index.row();
    int col = index.column();

    if (!model.at(row).canConvert<ScheduledTransactionTableObject>())
        return QVariant();

    ScheduledTransactionTableObject object = model.at(row).value<ScheduledTransactionTableObject>();

    int unit = walletModel->getOptionsModel()->getDisplayUnit();

    switch (role) {
    case Qt::DisplayRole:
    {
        // Time
        if (col == 0) {
            return object.time.toString();
        }
        // Amount
        if (col == 1) {

            QString amount = BitcoinUnits::formatWithUnit(unit, object.amount, false,
                    BitcoinUnits::separatorAlways);
            return amount;
        }
        // Hash
        if (col == 2) {
            return object.hash;
        }
    }
    case Qt::TextAlignmentRole:
    {
        // Time
        if (col == 0) {
            return int(Qt::AlignLeft | Qt::AlignVCenter);
        }
        // Total Withdrawn
        if (col == 1) {
            return int(Qt::AlignRight | Qt::AlignVCenter);
        }
        // Hash
        if (col == 2) {
            return int(Qt::AlignLeft | Qt::AlignVCenter);
        }
    }
    }
    return QVariant();
}

QVariant ScheduledTransactionTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Horizontal) {
            switch (section) {
            case 0:
                return QString("Time");
            case 1:
                return QString("Amount");
            case 2:
                return QString("TxID");
            }
        }
    }
    return QVariant();
}

void ScheduledTransactionTableModel::UpdateModel()
{
    beginResetModel();
    model.clear();
    endResetModel();

    if (vScheduled.empty())
        return;

    // Add to model
    beginInsertRows(QModelIndex(), model.size(), model.size() + vScheduled.size() - 1);
    for (const ScheduledTransaction& tx : vScheduled) {
        ScheduledTransactionTableObject object;

        // Insert new object into table
        object.hash = QString::fromStdString(tx.tx.GetHash().ToString());
        object.amount = tx.amount;
        object.time = QDateTime::fromMSecsSinceEpoch(tx.nTime);
        model.append(QVariant::fromValue(object));
    }
    endInsertRows();
}

void ScheduledTransactionTableModel::BroadcastScheduled()
{
    if (vScheduled.empty())
        return;

    QDateTime currentTime = QDateTime::currentDateTime();

    // Check for scheduled transactions that need to be broadcasted
    std::vector<ScheduledTransaction> vRemove;
    for (const ScheduledTransaction& tx : vScheduled) {
        // Check time
        if (currentTime.secsTo(QDateTime::fromMSecsSinceEpoch(tx.nTime)) < 0) {
            // TODO expire at a certain time and show error?
            //
            // TODO Broadcast
            // If broadcasted, remove
            vRemove.push_back(tx);
        }
    }
    for (const ScheduledTransaction& tx : vRemove) {
        Remove(tx);
    }
}

bool ScheduledTransactionTableModel::GetTxidAtRow(int row, uint256& hash) const
{
    if (row >= model.size())
        return false;

    if (!model[row].canConvert<ScheduledTransactionTableObject>())
        return false;

    ScheduledTransactionTableObject object = model[row].value<ScheduledTransactionTableObject>();

    hash = uint256S(object.hash.toStdString());

    return true;
}

void ScheduledTransactionTableModel::setWalletModel(WalletModel *model)
{
    this->walletModel = model;
}

void ScheduledTransactionTableModel::ScheduleTransaction(const ScheduledTransaction& tx)
{
    vScheduled.push_back(tx);
    UpdateModel();
}

void ScheduledTransactionTableModel::Remove(const ScheduledTransaction& tx)
{
    // TODO update container

    for (size_t i = 0; i < vScheduled.size(); i++) {
        if (vScheduled[i].nTime == tx.nTime && vScheduled[i].tx == tx.tx) {
            vScheduled[i] = vScheduled.back();
            vScheduled.pop_back();
            break;
        }
    }

    UpdateModel();
}
