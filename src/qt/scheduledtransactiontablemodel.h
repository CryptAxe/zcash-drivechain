// Copyright (c) 2020 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_SCHEDULEDTRANSACTIONTABLEMODEL_H
#define BITCOIN_SCHEDULEDTRANSACTIONTABLEMODEL_H

#include <QAbstractTableModel>
#include <QDateTime>
#include <QList>
#include <QString>

#include <amount.h>

class ScheduledTransaction;
class uint256;

class ClientModel;
class WalletModel;

QT_BEGIN_NAMESPACE
class QTimer;
QT_END_NAMESPACE

struct ScheduledTransactionTableObject
{
    QString hash;
    CAmount amount;
    QDateTime time;
};

class ScheduledTransactionTableModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit ScheduledTransactionTableModel(QObject *parent = 0);
    ~ScheduledTransactionTableModel();
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    bool GetTxidAtRow(int row, uint256& hash) const;
    void setWalletModel(WalletModel *model);
    void ScheduleTransaction(const ScheduledTransaction& tx);
    void Remove(const ScheduledTransaction& tx);

public Q_SLOTS:
    void UpdateModel();
    void BroadcastScheduled();

private:
    QList<QVariant> model;
    QTimer *pollTimer;

    WalletModel *walletModel = nullptr;

    std::vector<ScheduledTransaction> vScheduled;
};

#endif // BITCOIN_SCHEDULEDTRANSACTIONTABLEMODEL_H
