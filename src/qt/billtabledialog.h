// Copyright (c) 2021 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_BILLTABLEDIALOG_H
#define BITCOIN_BILLTABLEDIALOG_H

#include <QDialog>

namespace Ui {
class BillTableDialog;
}


const int COLUMN_DAY = 0;

class BillTableDialog : public QDialog
{
    Q_OBJECT

public:
    explicit BillTableDialog(QWidget *parent = nullptr);
    ~BillTableDialog();

private:
    Ui::BillTableDialog *ui;

    void ShowBills();
};

#endif // BITCOIN_BILLTABLEDIALOG_H
